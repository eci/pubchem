Code	Explanation	KeywordLinkOnSLE
MS	Mass spectrometry library	
PFAS	PFAS (per and polyfluorinated substances) list	https://zenodo.org/communities/norman-sle/search?page=1&size=20&q=PFAS
PCP	Personal Care Products lists	
Pharma	Pharmaceuticals	
Pest	Pesticides list	
Water	"List relevant for water resources (drinking, groundwater, wastewater)"	
Reg	Contains regulatory information	
NP	Natural products	
TPs	Transformation product information	
Toxins	"Toxins (e.g. natural toxins, neurotoxins)"	
Expt	Experimental data available	
Prio	Priority substances 	
Plastics	Chemicals associated with plastics	
Use	Usage data available	
Dust	List relevant for household dust	
Stds	List of analytical standards	
Pred	Predicted data available	
Expo	"Exposure data (e.g. tonnage, score)"	
Haz	Hazard data	
Lit	Contains literature references	
CCS	Ion Mobility / Collision Cross Section information	https://zenodo.org/communities/norman-sle/search?page=1&size=20&q=CCS
Surf	Surfactants	
Biomarkers	Biomarkers	
Smoke	Component of smoke	
Food	Relevant for food	
Vet	Vetenairy drugs	
HBM	Human biomonitoring	
DBP	Disinfection byproduct	
Tattoo	Tattoo ink ingredients	
Tire	Tire (tyre) related chemicals	
NMR	NMR data available	
UVCB	"Substances of Unknown or Variable Composition, Complex Reaction Products, or Biological Materials (UVCBs)"	
EDC	Endocrine disruptor	
