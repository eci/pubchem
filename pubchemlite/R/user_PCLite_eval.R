# Customizable MetFrag reporting functions
# Emma Schymanski, Todor Kondic
# 11 May 2020
# PubChemLite basic options
# Feb '21 extending to extra (exposomics) terms as well ... 

# AgroChemInfo, BioPathway, DrugMedicInfo, FoodRelated, 
# PharmacoInfo, SafetyInfo, ToxicityInfo, KnownUse
# DisorderDisease, Identification (exposomics version)

user_mk_cmpd_info <- function(cmpd_info,IKmatch=TRUE,extra_terms = c("none"),
                              skipMSMS=FALSE,XlogP=FALSE) {
    # set up the extra terms
    test_terms <- grep(TRUE, extra_terms %in% c("agro","bio","drug","food", "pharma", 
                                                "safety", "toxicity", "use", 
                                                "disease", "ident", "none"))
    if(length(test_terms)<1) {
        stop("Incorrect extra terms: select one or more of agro, bio, drug, 
             food, pharma, safety, toxicity, use, diseas, ident, none")
    }
    
    ## Set up new columns
    if (!skipMSMS) {
        cmpd_info$msms_avail <- FALSE
        cmpd_info$msms_peaks <- ""
    }
    ## Basic MetFrag summary: max score of 4 or 5 with FPSum/AnnoTypeCount
    cmpd_info$num_poss_IDs <- ""
    cmpd_info$max_Score <- ""
    cmpd_info$n_Score_GE4 <- ""
    cmpd_info$n_Score_GE3 <- ""
    cmpd_info$n_Score_GE2 <- ""
    ## Summary of top candidate and max metadata values 
    cmpd_info$CID_maxScore <- ""
    cmpd_info$SMILES_maxScore <- ""
    cmpd_info$InChIKey_maxScore <- ""
    cmpd_info$Name_maxScore <- ""
    cmpd_info$ExplPeaks_maxScore <- ""
    
    ## Max of individual categories
    ## MetFrag
    cmpd_info$max_NoExplPeaks <- ""
    cmpd_info$NumberPeaksUsed <- ""
    cmpd_info$max_FragmenterScore <- ""
    ## MoNA
    #cmpd_info$max_MetFusion <- ""
    cmpd_info$max_MoNAIndiv <- ""
    #PubChem Scores: PubMed_Count,Patent_Count,Annotation_Count
    # note for earliest PubChemLite, AnnoCount is FPSum, after AnnoTypeCount
    cmpd_info$max_PubMedCount <- ""
    cmpd_info$max_PatentCount <- ""
    cmpd_info$max_AnnoCount <- ""
    # Extra annotation scores: 
    if (length(grep("agro",extra_terms))>0) {
        cmpd_info$max_AgroChemInfo <- ""
    }
    if (length(grep("bio",extra_terms))>0) {
        cmpd_info$max_BioPathway <- ""
    }
    if (length(grep("drug",extra_terms))>0) {
        cmpd_info$max_DrugMedicInfo <- ""
    }
    if (length(grep("food",extra_terms))>0) {
        cmpd_info$max_FoodRelated <- ""
    }
    if (length(grep("pharma",extra_terms))>0) {
        cmpd_info$max_PharmacoInfo <- ""
    }
    if (length(grep("safety",extra_terms))>0) {
        cmpd_info$max_SafetyInfo <- ""
    }
    if (length(grep("toxicity",extra_terms))>0) {
        cmpd_info$max_ToxicityInfo <- ""
    }
    if (length(grep("use",extra_terms))>0) {
        cmpd_info$max_KnownUse <- ""
    }
    if (length(grep("disease",extra_terms))>0) {
        cmpd_info$max_DisorderDisease <- ""
    }
    if (length(grep("ident",extra_terms))>0) {
        cmpd_info$max_Identification <- ""
    }
    
    ## Summary of scores over all candidates
    #summary of scores over all candidates
    cmpd_info$cand_CIDs <- ""
    cmpd_info$cand_Scores <- ""
    cmpd_info$cand_NoExplPeaks <- ""
    cmpd_info$cand_FragmenterScore <- ""
    #MoNA
    #cmpd_info$cand_MetFusion <- ""
    cmpd_info$cand_MoNAIndiv <- ""
    # add XlogP if present
    if (XlogP) {
        cmpd_info$cand_XlogP <- ""
    }
    #PubChem data 
    cmpd_info$cand_PubMedCount <- ""
    cmpd_info$cand_PatentCount <- ""
    cmpd_info$cand_AnnoCount <- ""
    # extra annotation terms
    if (length(grep("agro",extra_terms))>0) {
        cmpd_info$cand_AgroChemInfo <- ""
    }
    if (length(grep("bio",extra_terms))>0) {
        cmpd_info$cand_BioPathway <- ""
    }
    if (length(grep("drug",extra_terms))>0) {
        cmpd_info$cand_DrugMedicInfo <- ""
    }
    if (length(grep("food",extra_terms))>0) {
        cmpd_info$cand_FoodRelated <- ""
    }
    if (length(grep("pharma",extra_terms))>0) {
        cmpd_info$cand_PharmacoInfo <- ""
    }
    if (length(grep("safety",extra_terms))>0) {
        cmpd_info$cand_SafetyInfo <- ""
    }
    if (length(grep("toxicity",extra_terms))>0) {
        cmpd_info$cand_ToxicityInfo <- ""
    }
    if (length(grep("use",extra_terms))>0) {
        cmpd_info$cand_KnownUse <- ""
    }
    if (length(grep("disease",extra_terms))>0) {
        cmpd_info$cand_DisorderDisease <- ""
    }
    if (length(grep("ident",extra_terms))>0) {
        cmpd_info$cand_Identification <- ""
    }
    
    if (IKmatch) {
        #TopCandMatch?
        cmpd_info$IKeyFBlock_match <- ""
        cmpd_info$IKeyFBlock_rank <- ""
    }
    
    return(cmpd_info)
}


# #LocalCSVScoreTerms <- "PubMed_Count,Patent_Count,FPSum"
# #LocalCSVScoreWeights <- ",1,1,1"
# 
# 
# user_loc_score_terms <- function(AnnoCount="AnnoTypeCount") {
#     list("PubMed_Count",
#          "Patent_Count",
#          "AnnoTypeCount")
# }
# 
# 
# user_loc_score_weights <- function() {
#     list(PubMed_Count=1,
#          Patent_Count=1,
#          AnnoTypeCount=1)
# }

# user_res_fn_suff <- function(i,cmpd_info) {
#     paste0(cmpd_info$ID[i],"_", cmpd_info$tag[i], "_",cmpd_info$set[i], "_",cmpd_info$adduct_type[i])
#     
# }


user_fill_cmpd_info <- function(mf_res,cmpd_info_row,pre2020=FALSE,
                                IKmatch=TRUE,IK="",extra_terms=c("none"),
                                XlogP=FALSE) {
    # set up the extra terms
    test_terms <- grep(TRUE, extra_terms %in% c("agro","bio","drug","food", "pharma", 
                                                "safety", "toxicity", "use",
                                                "disease", "ident", "none"))
    if(length(test_terms)<1) {
        stop("Incorrect extra terms: select one or more of agro, bio, drug, 
             food, pharma, safety, toxicity, use, disease, ident, none")
    }
    
    index_maxScore <- 1 #which(max(as.numeric(mf_res$Score))==as.numeric(mf_res$Score))
    cmpd_info_row <- as.list(cmpd_info_row)
    ## Number of candidates and score summaries
    #cmpd_info_row$msms_peaks <- mf_res$NumberPeaksUsed[index_maxScore]
    cmpd_info_row$num_poss_IDs <- length(mf_res$Score)
    cmpd_info_row$max_Score <- max(as.numeric(mf_res$Score))
    cmpd_info_row$n_Score_GE4 <- length(which(as.numeric(mf_res$Score)>=4))
    cmpd_info_row$n_Score_GE3 <- length(which(as.numeric(mf_res$Score)>=3))
    cmpd_info_row$n_Score_GE2 <- length(which(as.numeric(mf_res$Score)>=2))
    
    ## Summary of top candidate and max metadata values 
    cmpd_info_row$CID_maxScore <- mf_res$Identifier[index_maxScore]
    cmpd_info_row$SMILES_maxScore <- mf_res$SMILES[index_maxScore]
    cmpd_info_row$InChIKey_maxScore <- mf_res$InChIKey[index_maxScore]
    cmpd_info_row$Name_maxScore <- mf_res$CompoundName[index_maxScore]
    cmpd_info_row$ExplPeaks_maxScore <- mf_res$ExplPeaks[index_maxScore]
    ## Max of individual categories
    ## MetFrag
    cmpd_info_row$max_NoExplPeaks <- max(as.numeric(mf_res$NoExplPeaks))
    cmpd_info_row$NumberPeaksUsed <- max(as.numeric(mf_res$NumberPeaksUsed))
    cmpd_info_row$max_FragmenterScore <- max(as.numeric(mf_res$FragmenterScore))
    ## MoNA
    #cmpd_info_row$max_MetFusion <- max(as.numeric(mf_res$OfflineMetFusionScore))
    cmpd_info_row$max_MoNAIndiv <- max(as.numeric(mf_res$OfflineIndividualMoNAScore))
    #PubChem data
    cmpd_info_row$max_PubMedCount <- suppressWarnings(max(as.numeric(mf_res$PubMed_Count),na.rm=T))
    cmpd_info_row$max_PatentCount <- suppressWarnings(max(as.numeric(mf_res$Patent_Count),na.rm=T))
    if (pre2020) {
        cmpd_info_row$max_AnnoCount <- suppressWarnings(max(as.numeric(mf_res$FPSum),na.rm=T))
    } else {
        cmpd_info_row$max_AnnoCount <- suppressWarnings(max(as.numeric(mf_res$AnnoTypeCount),na.rm=T))
    }

    ## Summary of scores over all candidates
    cmpd_info_row$cand_CIDs <- paste(mf_res$Identifier,collapse=";")
    cmpd_info_row$cand_Scores <- paste(mf_res$Score,collapse=";")
    cmpd_info_row$cand_NoExplPeaks <- paste(mf_res$NoExplPeaks,collapse=";")
    cmpd_info_row$cand_FragmenterScore <- paste(mf_res$FragmenterScore,collapse=";")
    ## MoNA
    #cmpd_info_row$cand_MetFusion <- paste(mf_res$OfflineMetFusionScore,collapse=";")
    cmpd_info_row$cand_MoNAIndiv <- paste(mf_res$OfflineIndividualMoNAScore,collapse=";")
    ## XlogP
    if (XlogP) {
        cmpd_info_row$cand_XlogP <- paste(mf_res$XLogP,collapse=";")
    }
    ## PubChem data
    cmpd_info_row$cand_PubMedCount <- paste(mf_res$PubMed_Count,collapse=";")
    cmpd_info_row$cand_PatentCount <- paste(mf_res$Patent_Count,collapse=";")
    if (pre2020) {
        cmpd_info_row$cand_AnnoCount <- paste(mf_res$FPSum,collapse=";")
    } else {
        cmpd_info_row$cand_AnnoCount <- paste(mf_res$AnnoTypeCount,collapse=";")
    }
    #match?
    if (IKmatch && InChIKey_test(IK)) {
        IKeyFBlock_rank <- grep(strtrim(IK,14),mf_res$FirstBlock)
        if (length(IKeyFBlock_rank) > 0) {
            cmpd_info_row$IKeyFBlock_rank <- IKeyFBlock_rank
            if (IKeyFBlock_rank==1) {
                cmpd_info_row$IKeyFBlock_match <- TRUE
            } else {
                cmpd_info_row$IKeyFBlock_match <- FALSE
            }
        } else {
            cmpd_info_row$IKeyFBlock_rank <- NA
            cmpd_info_row$IKeyFBlock_match <- FALSE
        }
    } else if (IKmatch) {
        # if it's not a valid InChIKey we can't compare
        print(paste0("Input is not a valid InChIKey: ",IK))
        cmpd_info_row$IKeyFBlock_rank <- NA
        cmpd_info_row$IKeyFBlock_match <- FALSE
    }
    
    # extra terms
    # AgroChemInfo, BioPathway, DrugMedicInfo, FoodRelated, 
    # PharmacoInfo, SafetyInfo, ToxicityInfo, KnownUse
    # DisorderDisease, Identification
    
    if (length(grep("agro",extra_terms))>0) {
        cmpd_info_row$max_AgroChemInfo <- suppressWarnings(max(as.numeric(mf_res$AgroChemInfo),na.rm=T))
        cmpd_info_row$cand_AgroChemInfo <- paste(mf_res$AgroChemInfo,collapse=";")
    }
    if (length(grep("bio",extra_terms))>0) {
        cmpd_info_row$max_BioPathway <- suppressWarnings(max(as.numeric(mf_res$BioPathway),na.rm=T))
        cmpd_info_row$cand_BioPathway <- paste(mf_res$BioPathway,collapse=";")
    }
    if (length(grep("drug",extra_terms))>0) {
        cmpd_info_row$max_DrugMedicInfo <- suppressWarnings(max(as.numeric(mf_res$DrugMedicInfo),na.rm=T))
        cmpd_info_row$cand_DrugMedicInfo <- paste(mf_res$DrugMedicInfo,collapse=";")
    }
    if (length(grep("food",extra_terms))>0) {
        cmpd_info_row$max_FoodRelated <- suppressWarnings(max(as.numeric(mf_res$FoodRelated),na.rm=T))
        cmpd_info_row$cand_FoodRelated <- paste(mf_res$FoodRelated,collapse=";")
    }
    if (length(grep("pharma",extra_terms))>0) {
        cmpd_info_row$max_PharmacoInfo <- suppressWarnings(max(as.numeric(mf_res$PharmacoInfo),na.rm=T))
        cmpd_info_row$cand_PharmacoInfo <- paste(mf_res$PharmacoInfo,collapse=";")
    }
    if (length(grep("safety",extra_terms))>0) {
        cmpd_info_row$max_SafetyInfo <- suppressWarnings(max(as.numeric(mf_res$SafetyInfo),na.rm=T))
        cmpd_info_row$cand_SafetyInfo <- paste(mf_res$SafetyInfo,collapse=";")
    }
    if (length(grep("toxicity",extra_terms))>0) {
        cmpd_info_row$max_ToxicityInfo <- suppressWarnings(max(as.numeric(mf_res$ToxicityInfo),na.rm=T))
        cmpd_info_row$cand_ToxicityInfo <- paste(mf_res$ToxicityInfo,collapse=";")
    }
    if (length(grep("use",extra_terms))>0) {
        cmpd_info_row$max_KnownUse <- suppressWarnings(max(as.numeric(mf_res$KnownUse),na.rm=T))
        cmpd_info_row$cand_KnownUse <- paste(mf_res$KnownUse,collapse=";")
    }
    if (length(grep("disease",extra_terms))>0) {
        cmpd_info_row$max_DisorderDisease <- suppressWarnings(max(as.numeric(mf_res$DisorderDisease),na.rm=T))
        cmpd_info_row$cand_DisorderDisease <- paste(mf_res$DisorderDisease,collapse=";")
    }
    if (length(grep("ident",extra_terms))>0) {
        cmpd_info_row$max_Identification <- suppressWarnings(max(as.numeric(mf_res$Identification),na.rm=T))
        cmpd_info_row$cand_Identification <- paste(mf_res$Identification,collapse=";")
    }
    

    return(cmpd_info_row)
    
}



### CompTox-based functions (needed for CompTox Eval)

mk_cmpd_info.CompTox <- function(cmpd_info,IKmatch=TRUE,skipMSMS=FALSE) {

    ## Set up new columns
    if (!skipMSMS) {
        cmpd_info$msms_avail <- FALSE
        cmpd_info$msms_peaks <- ""
    }
    ## Basic MetFrag summary: max score of 5 with basic scoring terms
    cmpd_info$num_poss_IDs <- ""
    cmpd_info$max_Score <- ""
    cmpd_info$n_Score_GE4 <- ""
    cmpd_info$n_Score_GE3 <- ""
    cmpd_info$n_Score_GE2 <- ""
    ## Summary of top candidate and max metadata values 
    cmpd_info$ID_maxScore <- ""
    cmpd_info$SMILES_maxScore <- ""
    cmpd_info$InChIKey_maxScore <- ""
    cmpd_info$Name_maxScore <- ""
    cmpd_info$ExplPeaks_maxScore <- ""
    
    ## Max of individual categories
    ## MetFrag
    cmpd_info$max_NoExplPeaks <- ""
    cmpd_info$NumberPeaksUsed <- ""
    cmpd_info$max_FragmenterScore <- ""
    ## MoNA
    #cmpd_info$max_MetFusion <- ""
    cmpd_info$max_MoNAIndiv <- ""
    # #PubChem Scores: PubMed_Count,Patent_Count,Annotation_Count
    # # note for earliest PubChemLite, AnnoCount is FPSum, after AnnoTypeCount
    # cmpd_info$max_PubMedCount <- ""
    # cmpd_info$max_PatentCount <- ""
    # cmpd_info$max_AnnoCount <- ""
    
    #CompTox Scores: NUMBER_OF_PUBMED_ARTICLES, PUBCHEM_DATA_SOURCES, DATA_SOURCES
    # abbreviations: PubMedArticles, PubChemSources, DataSources
    cmpd_info$max_PubMedArticles <- ""
    cmpd_info$max_PubChemSources <- ""
    cmpd_info$max_DataSources <- ""
    
    ## Summary of scores over all candidates
    #summary of scores over all candidates
    cmpd_info$cand_IDs <- ""
    cmpd_info$cand_Scores <- ""
    cmpd_info$cand_NoExplPeaks <- ""
    cmpd_info$cand_FragmenterScore <- ""
    #MoNA
    #cmpd_info$cand_MetFusion <- ""
    cmpd_info$cand_MoNAIndiv <- ""
    # #PubChem data 
    # cmpd_info$cand_PubMedCount <- ""
    # cmpd_info$cand_PatentCount <- ""
    # cmpd_info$cand_FPSum <- ""
    #CompTox data 
    cmpd_info$cand_PubMedArticles <- "" #NUMBER_OF_PUBMED_ARTICLES
    cmpd_info$cand_PubChemSources <- "" #PUBCHEM_DATA_SOURCES
    cmpd_info$cand_DataSources <- "" #DATA_SOURCES

    if (IKmatch) {
        #TopCandMatch?
        cmpd_info$IKeyFBlock_match <- ""
        cmpd_info$IKeyFBlock_rank <- ""
    }
    
    return(cmpd_info)
}



fill_cmpd_info.CompTox <- function(mf_res,cmpd_info_row,IKmatch=TRUE,IK="") {

    index_maxScore <- 1 #which(max(as.numeric(mf_res$Score))==as.numeric(mf_res$Score))
    cmpd_info_row <- as.list(cmpd_info_row)
    ## Number of candidates and score summaries
    #cmpd_info_row$msms_peaks <- mf_res$NumberPeaksUsed[index_maxScore]
    cmpd_info_row$num_poss_IDs <- length(mf_res$Score)
    cmpd_info_row$max_Score <- max(as.numeric(mf_res$Score))
    cmpd_info_row$n_Score_GE4 <- length(which(as.numeric(mf_res$Score)>=4))
    cmpd_info_row$n_Score_GE3 <- length(which(as.numeric(mf_res$Score)>=3))
    cmpd_info_row$n_Score_GE2 <- length(which(as.numeric(mf_res$Score)>=2))
    
    ## Summary of top candidate and max metadata values 
    cmpd_info_row$ID_maxScore <- mf_res$MAPPED_DTXSID[index_maxScore]
    cmpd_info_row$SMILES_maxScore <- mf_res$SMILES[index_maxScore]
    cmpd_info_row$InChIKey_maxScore <- mf_res$InChIKey[index_maxScore]
    cmpd_info_row$Name_maxScore <- mf_res$CompoundName[index_maxScore]
    cmpd_info_row$ExplPeaks_maxScore <- mf_res$ExplPeaks[index_maxScore]
    ## Max of individual categories
    ## MetFrag
    cmpd_info_row$max_NoExplPeaks <- max(as.numeric(mf_res$NoExplPeaks))
    cmpd_info_row$NumberPeaksUsed <- max(as.numeric(mf_res$NumberPeaksUsed))
    cmpd_info_row$max_FragmenterScore <- max(as.numeric(mf_res$FragmenterScore))
    ## MoNA
    #cmpd_info_row$max_MetFusion <- max(as.numeric(mf_res$OfflineMetFusionScore))
    cmpd_info_row$max_MoNAIndiv <- max(as.numeric(mf_res$OfflineIndividualMoNAScore))
    # #PubChem data
    # cmpd_info_row$max_PubMedCount <- suppressWarnings(max(as.numeric(mf_res$PubMed_Count),na.rm=T))
    # cmpd_info_row$max_PatentCount <- suppressWarnings(max(as.numeric(mf_res$Patent_Count),na.rm=T))
    # if (pre2020) {
    #     cmpd_info_row$max_AnnoCount <- suppressWarnings(max(as.numeric(mf_res$FPSum),na.rm=T))
    # } else {
    #     cmpd_info_row$max_AnnoCount <- suppressWarnings(max(as.numeric(mf_res$AnnoTypeCount),na.rm=T))
    # }
    #CompTox Scores: NUMBER_OF_PUBMED_ARTICLES, PUBCHEM_DATA_SOURCES, DATA_SOURCES
    # abbreviations: PubMedArticles, PubChemSources, DataSources
    cmpd_info_row$max_PubMedArticles <- suppressWarnings(max(as.numeric(mf_res$NUMBER_OF_PUBMED_ARTICLES),na.rm=T))
    cmpd_info_row$max_PubChemSources <- suppressWarnings(max(as.numeric(mf_res$PUBCHEM_DATA_SOURCES),na.rm=T))
    cmpd_info_row$max_DataSources <- suppressWarnings(max(as.numeric(mf_res$DATA_SOURCES),na.rm=T))
    
    
    ## Summary of scores over all candidates
    cmpd_info_row$cand_IDs <- paste(mf_res$MAPPED_DTXSID,collapse=";")
    cmpd_info_row$cand_Scores <- paste(mf_res$Score,collapse=";")
    cmpd_info_row$cand_NoExplPeaks <- paste(mf_res$NoExplPeaks,collapse=";")
    cmpd_info_row$cand_FragmenterScore <- paste(mf_res$FragmenterScore,collapse=";")
    ## MoNA
    #cmpd_info_row$cand_MetFusion <- paste(mf_res$OfflineMetFusionScore,collapse=";")
    cmpd_info_row$cand_MoNAIndiv <- paste(mf_res$OfflineIndividualMoNAScore,collapse=";")
    # ## PubChem data
    # cmpd_info_row$cand_PubMedCount <- paste(mf_res$PubMed_Count,collapse=";")
    # cmpd_info_row$cand_PatentCount <- paste(mf_res$Patent_Count,collapse=";")
    # if (pre2020) {
    #     cmpd_info_row$cand_AnnoCount <- paste(mf_res$FPSum,collapse=";")
    # } else {
    #     cmpd_info_row$cand_AnnoCount <- paste(mf_res$AnnoTypeCount,collapse=";")
    # }
    #CompTox Scores: NUMBER_OF_PUBMED_ARTICLES, PUBCHEM_DATA_SOURCES, DATA_SOURCES
    # abbreviations: PubMedArticles, PubChemSources, DataSources
    cmpd_info_row$cand_PubMedArticles <- paste(mf_res$NUMBER_OF_PUBMED_ARTICLES,collapse=";")
    cmpd_info_row$cand_PubChemSources <- paste(mf_res$PUBCHEM_DATA_SOURCES,collapse=";")
    cmpd_info_row$cand_DataSources <- paste(mf_res$DATA_SOURCES,collapse=";")
    
    #match?
    if (IKmatch && InChIKey_test(IK)) {
        IKeyFBlock_rank <- grep(strtrim(IK,14),strtrim(mf_res$INCHIKEY_DTXCID,14))
        if (length(IKeyFBlock_rank) > 0) {
            cmpd_info_row$IKeyFBlock_rank <- IKeyFBlock_rank
            if (IKeyFBlock_rank==1) {
                cmpd_info_row$IKeyFBlock_match <- TRUE
            } else {
                cmpd_info_row$IKeyFBlock_match <- FALSE
            }
        } else {
            cmpd_info_row$IKeyFBlock_rank <- NA
            cmpd_info_row$IKeyFBlock_match <- FALSE
        }
    } else if (IKmatch) {
        # if it's not a valid InChIKey we can't compare
        print(paste0("Input is not a valid InChIKey: ",IK))
        cmpd_info_row$IKeyFBlock_rank <- NA
        cmpd_info_row$IKeyFBlock_match <- FALSE
    }
    
    return(cmpd_info_row)
    
}



### Full PubChem-based functions (needed for FullPC Eval)

mk_cmpd_info.FullPC <- function(cmpd_info,IKmatch=TRUE,skipMSMS=FALSE) {
    
    ## Set up new columns
    if (!skipMSMS) {
        cmpd_info$msms_avail <- FALSE
        cmpd_info$msms_peaks <- ""
    }
    ## Basic MetFrag summary: max score of 4 with basic scoring terms
    cmpd_info$num_poss_IDs <- ""
    cmpd_info$max_Score <- ""
    cmpd_info$n_Score_GE4 <- ""
    cmpd_info$n_Score_GE3 <- ""
    cmpd_info$n_Score_GE2 <- ""
    ## Summary of top candidate and max metadata values 
    cmpd_info$ID_maxScore <- ""
    cmpd_info$SMILES_maxScore <- ""
    cmpd_info$InChIKey_maxScore <- ""
    cmpd_info$Name_maxScore <- ""
    cmpd_info$ExplPeaks_maxScore <- ""
    
    ## Max of individual categories
    ## MetFrag
    cmpd_info$max_NoExplPeaks <- ""
    cmpd_info$NumberPeaksUsed <- ""
    cmpd_info$max_FragmenterScore <- ""
    ## MoNA
    #cmpd_info$max_MetFusion <- ""
    cmpd_info$max_MoNAIndiv <- ""
    # #PubChem Scores: PubMed_Count,Patent_Count
    cmpd_info$max_PubMedCount <- ""
    cmpd_info$max_PatentCount <- ""

    # ## Summary of scores over all candidates
    # #summary of scores over all candidates
    # cmpd_info$cand_IDs <- ""
    # cmpd_info$cand_Scores <- ""
    # cmpd_info$cand_NoExplPeaks <- ""
    # cmpd_info$cand_FragmenterScore <- ""
    # #MoNA
    # #cmpd_info$cand_MetFusion <- ""
    # cmpd_info$cand_MoNAIndiv <- ""
    # # #PubChem data 
    # cmpd_info$cand_PubMedCount <- ""
    # cmpd_info$cand_PatentCount <- ""

    if (IKmatch) {
        #TopCandMatch?
        cmpd_info$IKeyFBlock_match <- ""
        cmpd_info$IKeyFBlock_rank <- ""
    }
    
    return(cmpd_info)
}



fill_cmpd_info.FullPC <- function(mf_res,cmpd_info_row,IKmatch=TRUE,IK="") {
    
    index_maxScore <- 1 #which(max(as.numeric(mf_res$Score))==as.numeric(mf_res$Score))
    cmpd_info_row <- as.list(cmpd_info_row)
    ## Number of candidates and score summaries
    #cmpd_info_row$msms_peaks <- mf_res$NumberPeaksUsed[index_maxScore]
    cmpd_info_row$num_poss_IDs <- length(mf_res$Score)
    cmpd_info_row$max_Score <- max(as.numeric(mf_res$Score))
    cmpd_info_row$n_Score_GE4 <- length(which(as.numeric(mf_res$Score)>=4))
    cmpd_info_row$n_Score_GE3 <- length(which(as.numeric(mf_res$Score)>=3))
    cmpd_info_row$n_Score_GE2 <- length(which(as.numeric(mf_res$Score)>=2))
    
    ## Summary of top candidate and max metadata values 
    cmpd_info_row$ID_maxScore <- mf_res$Identifier[index_maxScore]
    cmpd_info_row$SMILES_maxScore <- mf_res$SMILES[index_maxScore]
    cmpd_info_row$InChIKey_maxScore <- mf_res$InChIKey[index_maxScore]
    cmpd_info_row$Name_maxScore <- mf_res$IUPACName[index_maxScore]
    cmpd_info_row$ExplPeaks_maxScore <- mf_res$ExplPeaks[index_maxScore]
    ## Max of individual categories
    ## MetFrag
    cmpd_info_row$max_NoExplPeaks <- max(as.numeric(mf_res$NoExplPeaks))
    cmpd_info_row$NumberPeaksUsed <- max(as.numeric(mf_res$NumberPeaksUsed))
    cmpd_info_row$max_FragmenterScore <- max(as.numeric(mf_res$FragmenterScore))
    ## MoNA
    #cmpd_info_row$max_MetFusion <- max(as.numeric(mf_res$OfflineMetFusionScore))
    cmpd_info_row$max_MoNAIndiv <- max(as.numeric(mf_res$OfflineIndividualMoNAScore))
    # #PubChem data
    cmpd_info_row$max_PubMedCount <- suppressWarnings(max(as.numeric(mf_res$PubChemNumberPubMedReferences),na.rm=T))
    cmpd_info_row$max_PatentCount <- suppressWarnings(max(as.numeric(mf_res$PubChemNumberPatents),na.rm=T))
    
    # ## Summary of scores over all candidates
    # cmpd_info_row$cand_IDs <- paste(mf_res$Identifier,collapse=";")
    # cmpd_info_row$cand_Scores <- paste(mf_res$Score,collapse=";")
    # cmpd_info_row$cand_NoExplPeaks <- paste(mf_res$NoExplPeaks,collapse=";")
    # cmpd_info_row$cand_FragmenterScore <- paste(mf_res$FragmenterScore,collapse=";")
    # ## MoNA
    # #cmpd_info_row$cand_MetFusion <- paste(mf_res$OfflineMetFusionScore,collapse=";")
    # cmpd_info_row$cand_MoNAIndiv <- paste(mf_res$OfflineIndividualMoNAScore,collapse=";")
    # # ## PubChem data
    # cmpd_info_row$cand_PubMedCount <- paste(mf_res$PubChemNumberPubMedReferences,collapse=";")
    # cmpd_info_row$cand_PatentCount <- paste(mf_res$PubChemNumberPatents,collapse=";")

    #match?
    if (IKmatch && InChIKey_test(IK)) {
        IKeyFBlock_rank <- grep(strtrim(IK,14),strtrim(mf_res$InChIKey,14))
        if (length(IKeyFBlock_rank) > 0) {
            cmpd_info_row$IKeyFBlock_rank <- IKeyFBlock_rank
            if (IKeyFBlock_rank==1) {
                cmpd_info_row$IKeyFBlock_match <- TRUE
            } else {
                cmpd_info_row$IKeyFBlock_match <- FALSE
            }
        } else {
            cmpd_info_row$IKeyFBlock_rank <- NA
            cmpd_info_row$IKeyFBlock_match <- FALSE
        }
    } else if (IKmatch) {
        # if it's not a valid InChIKey we can't compare
        print(paste0("Input is not a valid InChIKey: ",IK))
        cmpd_info_row$IKeyFBlock_rank <- NA
        cmpd_info_row$IKeyFBlock_match <- FALSE
    }
    
    return(cmpd_info_row)
    
}


#### CCSBase functions ####

# pred_CCS_A2_[M+H]+    pred_CCS_A2_[M+K]+  pred_CCS_A2_[M+NH4]+ 
# pred_CCS_A2_[M+Na-2H]-	pred_CCS_A2_[M+Na]+ pred_CCS_A2_[M-H]-	
# pred_CCS_A2_[M]+	pred_CCS_A2_[M]-

mk_cmpd_info.CCSbase <- function(cmpd_info,adducts=c("[M+H]+")) {
    CCS_headers <- c("pred_CCS_A2_[M+H]+","pred_CCS_A2_[M+K]+","pred_CCS_A2_[M+NH4]+",
                     "pred_CCS_A2_[M+Na-2H]-","pred_CCS_A2_[M+Na]+",
                     "pred_CCS_A2_[M-H]-","pred_CCS_A2_[M]+","pred_CCS_A2_[M]-")
    CCS_adducts <- c("[M+H]+","[M+K]+","[M+NH4]+","[M+Na-2H]-","[M+Na]+","[M-H]-",
                     "[M]+","[M]-") 
    # set up the extra terms
    test_terms <- grep(TRUE, adducts %in% CCS_adducts)
    if(length(test_terms)<1) {
        stop("Incorrect adducts: select one or more of [M+H]+,[M+K]+,[M+NH4]+,
             [M+Na-2H]-,[M+Na]+,[M-H]-,[M]+,[M]-")
    }
    
    ## Summary of CCS over all candidates
    if (length(grep("[M+H]+",adducts,fixed=TRUE))>0) {
        cmpd_info$'cand_pred_CCS_A2_[M+H]+' <- ""
    }
    if (length(grep("[M+K]+",adducts,fixed=TRUE))>0) {
        cmpd_info$'cand_pred_CCS_A2_[M+K]+' <- ""
    }
    if (length(grep("[M+NH4]+",adducts,fixed=TRUE))>0) {
        cmpd_info$'cand_pred_CCS_A2_[M+NH4]+' <- ""
    }
    if (length(grep("[M+Na-2H]-",adducts,fixed=TRUE))>0) {
        cmpd_info$'cand_pred_CCS_A2_[M+Na-2H]-' <- ""
    }
    if (length(grep("[M+Na]+",adducts,fixed=TRUE))>0) {
        cmpd_info$'cand_pred_CCS_A2_[M+Na]+' <- ""
    }
    if (length(grep("[M-H]-",adducts,fixed=TRUE))>0) {
        cmpd_info$'cand_pred_CCS_A2_[M-H]-' <- ""
    }
    if (length(grep("[M]+",adducts,fixed=TRUE))>0) {
        cmpd_info$'cand_pred_CCS_A2_[M]+' <- ""
    }
    if (length(grep("[M]-",adducts,fixed=TRUE))>0) {
        cmpd_info$'cand_pred_CCS_A2_[M]-' <- ""
    }

    return(cmpd_info)
}



fill_cmpd_info.CCSbase <- function(mf_res,cmpd_info_row,adducts=c("[M+H]+")) {
    # set up the extra terms
    CCS_headers <- c("pred_CCS_A2_[M+H]+","pred_CCS_A2_[M+K]+","pred_CCS_A2_[M+NH4]+",
                     "pred_CCS_A2_[M+Na-2H]-","pred_CCS_A2_[M+Na]+",
                     "pred_CCS_A2_[M-H]-","pred_CCS_A2_[M]+","pred_CCS_A2_[M]-")
    CCS_adducts <- c("[M+H]+","[M+K]+","[M+NH4]+","[M+Na-2H]-","[M+Na]+","[M-H]-",
                     "[M]+","[M]-") 
    # set up the extra terms
    test_terms <- grep(TRUE, adducts %in% CCS_adducts)
    if(length(test_terms)<1) {
        stop("Incorrect adducts: select one or more of [M+H]+,[M+K]+,[M+NH4]+,
             [M+Na-2H]-,[M+Na]+,[M-H]-,[M]+,[M]-")
    }

    # now add the terms
    if (length(grep("[M+H]+",adducts,fixed=TRUE))>0) {
        cmpd_info_row$'cand_pred_CCS_A2_[M+H]+' <- paste(mf_res$'pred_CCS_A2_[M+H]+',collapse=";")
    }
    if (length(grep("[M+K]+",adducts,fixed=TRUE))>0) {
        cmpd_info_row$'cand_pred_CCS_A2_[M+K]+' <- paste(mf_res$'pred_CCS_A2_[M+K]+',collapse=";")
    }
    if (length(grep("[M+NH4]+",adducts,fixed=TRUE))>0) {
        cmpd_info_row$'cand_pred_CCS_A2_[M+NH4]+' <- paste(mf_res$'pred_CCS_A2_[M+NH4]+',collapse=";")
    }
    if (length(grep("[M+Na-2H]-",adducts,fixed=TRUE))>0) {
        cmpd_info_row$'cand_pred_CCS_A2_[M+Na-2H]-' <- paste(mf_res$'pred_CCS_A2_[M+Na-2H]-',collapse=";")
    }
    if (length(grep("[M+Na]+",adducts,fixed=TRUE))>0) {
        cmpd_info_row$'cand_pred_CCS_A2_[M+Na]+' <- paste(mf_res$'pred_CCS_A2_[M+Na]+',collapse=";")
    }
    if (length(grep("[M-H]-",adducts,fixed=TRUE))>0) {
        cmpd_info_row$'cand_pred_CCS_A2_[M-H]-' <- paste(mf_res$'pred_CCS_A2_[M-H]-',collapse=";")
    }
    if (length(grep("[M]+",adducts,fixed=TRUE))>0) {
        cmpd_info_row$'cand_pred_CCS_A2_[M]+' <- paste(mf_res$'pred_CCS_A2_[M]+',collapse=";")
    }
    if (length(grep("[M]-",adducts,fixed=TRUE))>0) {
        cmpd_info_row$'cand_pred_CCS_A2_[M]-' <- paste(mf_res$'pred_CCS_A2_[M]-',collapse=";")
    }
    
    
    return(cmpd_info_row)
    
}


