#!/usr/bin/perl

use strict;
use warnings;

# Structure-based PFAS breakdown
#   Perfluoroalkyl acids
#   Perfluoroalkyl acid precurors
#   Polyfluoroalkyl acids
#   Select other PFAS
#     Perfluoropolyethers (PFPEs)
#     Side-chain fluorinated aromatics
#     Perfluoroalkanes
#     Perfluoroalkyl-tert-amines
#     Perfluoroalkylethers
#     Others

my $nexamples = 20;  # Number of examples to provide
my $ex_ratio = 1 / ( $nexamples - 1 );
my $url_prefix = "https://pubchem.ncbi.nlm.nih.gov/#collection=compound&query=";


my %tree = ();


#
# Read in PFAS classification data from PubChem FTP file processing
#
my %pfast = ();  # PFAS type
my @l = (
        "Organofluorine compounds\tFluorinated aliphatic substances\tFluorinated aliphatic substances that have a fully fluorinated methyl or methylene carbon atom",  #0
        "Organofluorine compounds\tFluorinated aliphatic substances\tOther fluorinated aliphatic substances that do NOT have a fully fluorinated methyl or methylene carbon atom",  #1
        "Organofluorine compounds\tFluorinated aromatic substances\t(Non-)Fluorinated aromatic ring(s) with fluorinated aliphatic side chain(s) that do NOT have a fully fluorinated methyl or methylene carbon atom",  #4
        "Organofluorine compounds\tFluorinated aromatic substances\tFluorinated aromatic ring(s) with fluorinated aliphatic side chain(s) that have a fully fluorinated methyl or methylene carbon atom",  #3
        "Organofluorine compounds\tFluorinated aromatic substances\tFluorinated aromatic ring(s) with non-fluorinated aliphatic side chain(s)",  #5
        "Organofluorine compounds\tFluorinated aromatic substances\tFluorinated aromatic substances without a side chain",  #6
        "Organofluorine compounds\tFluorinated aromatic substances\tNon-fluorinated aromatic ring(s) with fluorinated aliphatic side chain(s) that have fully fluorinated methyl or methylene carbon atom",  #2
        "Organofluorine compounds\tOther fluorinated substances",  #7
        "Other diverse fluorinated compounds"  #8
        );
my @n = ( 0, 0, 0, 0, 0, 0, 0, 0, 0 );
#open( TMP, "> otherpfas_data.tsv" ) || die "Unable to write tmp file\n";
open( TSV, "gunzip < pfas_frag2_set1.tsv.gz |" ) || die "Unable to read pfas_frag2_set.tsv\n";
open( UMF, "| gzip > treefluoroclassification.tsv.gz" ) || die "Unable to write: treefluoroclassification.tsv\n";
while ( $_ = <TSV> ) {
  chop;

  # Is this a header line?
  if ( substr( $_, 0, 3 ) eq "CID" ) {
    $_ = <TSV>;  # Skip also the Description line
    next;
  }


  my ( $cid, $mf, $ms, $contains_aromatic_atom, $contains_nonaromatic_atom, $contains_fluorinated_aliphatic_atom, $contains_fluorinated_unsaturated_atom, $contains_fluorinated_aromatic_atom, $contains_oecd_pfas_atom, $nf, $ninorg, $nncf, $el, $bel, $at )  = split( /	/, $_, 15 );

  my @el = split( / /, $el );
  my @bel = split( / /, $bel );
  my @at = split( / /, $at );


  my $sms = "";
  if ( $ms <= 250.0 ) {
    $sms = "Exact mass range 1-250";
  } elsif ( $ms <= 500.0 ) {
    $sms = "Exact mass range 250-500";
  } elsif ( $ms <= 750.0 ) {
    $sms = "Exact mass range 500-750";
  } elsif ( $ms <= 1000.0 ) {
    $sms = "Exact mass range 750-1000";
  } else {
    $sms = "Exact mass range >1000";
  }

  my $snf = "";
  if ( $nf > 15 ) {
    $snf = "Contains >15 Fluorine atoms\t" . $sms;
  } elsif ( $nf == 1 ) {
    $snf = "Contains 01 Fluorine atom\t" . $sms;
  } else {
    $snf = sprintf( "Contains %02d Fluorine atoms\t", $nf ) . $sms;
  }

  my $did_already = 0;
  if ( ! $contains_aromatic_atom && $contains_fluorinated_aliphatic_atom && ! $contains_oecd_pfas_atom ) {
    print UMF "$l[1]\t$snf\t$cid\n";
    $n[1]++;
    $did_already++;
  } elsif ( ! $contains_aromatic_atom  && $contains_oecd_pfas_atom ) {
    print UMF "$l[0]\t$snf\t$cid\n";
    $n[0]++;
    $did_already++;
  } elsif ( $contains_aromatic_atom && ! $contains_fluorinated_aromatic_atom && $contains_oecd_pfas_atom ) {
    print UMF "$l[6]\t$snf\t$cid\n";
    $n[6]++;
    $did_already++;
  } elsif ( $contains_fluorinated_aromatic_atom && $contains_oecd_pfas_atom ) {
    print UMF "$l[3]\t$snf\t$cid\n";
    $n[3]++;
    $did_already++;
  } elsif ( $contains_aromatic_atom && $contains_fluorinated_aliphatic_atom && ! $contains_oecd_pfas_atom ) {
    print UMF "$l[2]\t$snf\t$cid\n";
    $n[2]++;
    $did_already++;
  } elsif ( $contains_fluorinated_aromatic_atom && ! $contains_nonaromatic_atom ) {
    print UMF "$l[5]\t$snf\t$cid\n";
    $n[5]++;
    $did_already++;
  } elsif ( $contains_fluorinated_aromatic_atom && ! $contains_fluorinated_aliphatic_atom ) {
    print UMF "$l[4]\t$snf\t$cid\n";
    $n[4]++;
    $did_already++;
#  } else {
#    if ( $nf == 1 ) {
#      $snf = "Contains 1 Fluorine atom\t" . $sms;
#    } else {
#      $snf = sprintf( "Contains %d Fluorine atoms\t", $nf ) . $sms;
#    }
#
#    if ( $nncf ) {
#      $did_already_het = 1;
#      print UMF "$l[7]\t$snf\t$cid\n";
#      $n[7]++;
#
#      print TMP "0 :: $_\n";
#    } elsif ( $ninorg ) {
#      $did_already_inorg = 1;
#      print UMF "$l[8]\t$snf\t$cid\n";
#      $n[8]++;
#
#      print TMP "1 :: $_\n";
#    } else {
#      $did_already_inorg = 1;
#      print UMF "$l[8]\t$snf\t$cid\n";
#      $n[8]++;
##      print ":: WARNING :: Case of unhandled Other fluorinated substance in PFAS classification :: $_\n";
#    }
  }
 
  my $contains_c = 0;
  foreach my $el ( @el ) {
    if ( substr( $el, 0, 2 ) eq "C:" ) {
      $contains_c++;
      last;
    }
  }

  if ( ! $contains_c || $ninorg ) {
    my $do_once = 1;
    foreach my $el ( @el ) {
      my @tel = split( /\:/, $el, 2 );
      if ( ! ( $tel[0] eq "H" || $tel[0] eq "C" || $tel[0] eq "N" || $tel[0] eq "O" || $tel[0] eq "F" || $tel[0] eq "P" || $tel[0] eq "S" || $tel[0] eq "Si" || $tel[0] eq "Cl" || $tel[0] eq "Br" || $tel[0] eq "I" ) ) {
        my $v = $l[8] . "\tContains non-organic element\tContains " . $tel[0] . " atom";
#        print UMF "$v\t$snf\t$cid\n";
        $did_already++;
#        if ( $do_once ) { $do_once = 0;  print TMP "0 :: $_\n"; }

        if ( exists( $tree{ $v }{ $cid } ) ) {
          print STDERR ":: ERROR :: Duplicate case of \"$v\" for CID:$cid\n";
        } else {
          $tree{ $v }{ $cid } = $snf;
        }
      }
    }

    foreach my $tbel ( @bel ) {
      if ( substr( $tbel, 0, 2 ) eq "F:" ) {
        my @tel = split( /\:/, $tbel, 3 );
        if ( $tel[1] ne "C" ) {
          my $v = $l[8] . "\tContains fluorine bond to non-carbon element\tContains a F-" . $tel[1] . " bond";
#          print UMF "$v\t$snf\t$cid\n";
          $did_already++;
#          if ( $do_once ) { $do_once = 0;  print TMP "0 :: $_\n"; }

          if ( exists( $tree{ $v }{ $cid } ) ) {
            print STDERR ":: ERROR :: Duplicate case of \"$v\" for CID:$cid\n";
          } else {
            $tree{ $v }{ $cid } = $snf;
          }
        }
      }
    }

#    if ( $do_once ) { print TMP "1 :: $contains_c : $ninorg :: $_\n"; }
    $n[8]++;
  } elsif ( $nncf ) {
    my $do_once = 1;
    foreach my $tbel ( @bel ) {
      if ( substr( $tbel, 0, 2 ) eq "F:" ) {
        my @tel = split( /\:/, $tbel );
        if ( $tel[1] ne "C" ) {
          my $v = $l[7] . "\tContains a F-" . $tel[1] . " bond";
          print UMF "$v\t$snf\t$cid\n";
          $did_already++;
#          if ( $do_once ) { $do_once = 0;  print TMP "2 :: $_\n"; }
        }
      }
    }

#    if ( $do_once ) { print TMP "3 :: $nncf :: $_\n"; }
    $n[7]++;
  }

  if ( ! $did_already ) {
    print ":: WARNING :: Case of unhandled Other fluorinated substance in PFAS classification :: $_\n";
  }
}
close( TSV );
#close( TMP );


print "\n\n";
for ( my $i = 0;  $i < 9;  $i++ ) {
  print "$n[$i]\t$l[$i]\n";
}
print "\n\n";

my %nt = ();
foreach my $t ( keys( %tree ) ) {
  my @cid = keys( %{ $tree{ $t } } );
  my $n = @cid;

  my @v = split( /	/, $t );
  my $e = pop( @v );

  my $nl = "";
  if ( $n > 100000 ) {
    $nl = join( "\t", @v, "Count of molecules >100000", $e );
    foreach my $cid ( @cid ) {
      print UMF "$nl\t$tree{$t}{$cid}\t$cid\n";
    }
  } elsif ( $n > 10000 ) {
    $nl = join( "\t", @v, "Count of molecules 10001-100000", $e );
    foreach my $cid ( @cid ) {
      print UMF "$nl\t$tree{$t}{$cid}\t$cid\n";
    }
  } elsif ( $n > 1000 ) {
    $nl = join( "\t", @v, "Count of molecules 01001-10000", $e );
    foreach my $cid ( @cid ) {
      my ( $t1, $t2 ) = split( /	/, $tree{$t}{$cid}, 2 );
      print UMF "$nl\t$t1\t$cid\n";
    }
  } elsif ( $n > 100 ) {
    $nl = join( "\t", @v, "Count of molecules 00101-1000", $e );
    foreach my $cid ( @cid ) {
      my ( $t1, $t2 ) = split( /	/, $tree{$t}{$cid}, 2 );
      print UMF "$nl\t$t1\t$cid\n";
    }
  } elsif ( $n > 10 ) {
    $nl = join( "\t", @v, "Count of molecules 00011-100", $e );
    foreach my $cid ( @cid ) {
      print UMF "$nl\t$cid\n";
    }
  } else {
    $nl = join( "\t", @v, "Count of molecules 00001-10", $e );
    foreach my $cid ( @cid ) {
      print UMF "$nl\t$cid\n";
    }
  }

  $nt{ $nl } = $n;
#  print "$n\t$t\n";
}

close( UMF );

foreach my $t ( sort( keys( %nt ) ) ) {
  print "$nt{$t}\t$t\n";
}


#print "$n[0]\tOther fluorinated aliphatic substances that do not have a fully fluorinated methyl or methylene carbon atom\n";
#print "$n[1]\tFluorinated aliphatic substances that have a fully fluorinated methyl or methylene carbon atom\n";
#print "$n[2]\tNon-fluorinated aromatic ring(s) with fluorinated aliphatic side chain(s) that have fully fuorinated methyl or methylene carbon atom\n";
#print "$n[3]\tFluorinated aromatic ring(s) with fluorinated aliphatic side chain(s) that have a fully fluorinated methyl or methylene carbon atom\n";
#print "$n[4]\tFluorinated aromatic substances\t(Non-)Fluorinated aromatic ring(s) with fluorinated aliphatic side chain(s) that do NOT have a fully fluorinated methyl or methylene carbon atom\n";
#print "$n[5]\tFluorinated aromatic ring(s) with non-fluorinated aliphatic side chain(s)\n";
#print "$n[6]\tFluorinated aromatic substances without a side chain\n";
#print "$n[7]\tOther fluorinated substances\n";

#exit( 1 );



#
# Read in the PFAS data from PubChem FTP file processing
#
my %fragTypeMF = ();
my %binMassMF = ();
my %cmf = ();
my %c = ();
my %t = ();
my %mf = ();
my %of = ();  # CIDs containing Non CF2/CF3 fragments (w/ fragment count)
my %oecd = ();  # Basic info for the OECD tree

#my @p = (
#          "Perfluoroalkyl carboxylic acids (PFCAs)",
#          "Perfluoroalkane sulfonic acids (PFSAs)",
#          "Perfluoroalkyl phosphonic acids (PFPAs)",
#          "Perfluoroalkyl phosphinic acids (PFPIAs)",
#          "Perfluoroalkylether carboxylic acids (PFECAs)",
#          "Perfluoroalkylether sulfonic acids (PFESAs)",
#          "Perfluoroalkyl dicarboxylic acids (PFdiCAs)",
#          "Perfluoroalkane disulfonic acids (PFdiSAs)",
#          "Perfluoroalkane sulfinic acids (PFSIAs)"
#        );

open( TSV, "gunzip < pfas_frag2_set2.tsv.gz |" ) || die "Unable to read pfas_frag2_set2.tsv\n";
while ( $_ = <TSV> ) {
  chop;

  #CID     PFASfragmentMF  PFASfragORmol   PFASfragmentType        PFASfragmentUnsaturatedDegree   PFASfragmentAtomCount   PFASfragmentBondCount   PFASfragmentCarbonCount PFASfragmentFluorineCount       PFASfragmentMass CidMonoIsotopicMass CidMF
  my @tmp = split( /	/, $_, 12 );

  # Is this a header line?
  if ( $tmp[0] eq "CID" ) {
    $_ = <STDIN>;  # Skip also the Description line
    next;
  }

  # [00] "CID\t",
  # [01] "PFASfragmentMF\t",
  # [02] "PFASfragORmol\t",
  # [03] "PFASfragmentType\t",
  # [04] "PFASfragmentUnsaturatedDegree\t",
  # [05] "PFASfragmentAtomCount\t",
  # [06] "PFASfragmentBondCount\t",
  # [07] "PFASfragmentCarbonCount\t",
  # [08] "PFASfragmentFluorineCount\t",
  # [09] "PFASfragmentMass\t",
  # [10] "CidMonoIsotopicMass\t",
  # [11] "CidMF\n";

  if ( ! defined( $tmp[10] ) ) {
    print STDERR ":: WARNING :: tmp[10] not defined :: $_\n";
  }


  # Bin the mass w/ MF and then by CID
  my $mass_bin = int( $tmp[10] / 100.0 );
  my $fmass_bin = int( $tmp[9] / 100.0 );
  $binMassMF{ $mass_bin }{ $fmass_bin }{ $tmp[1] }{ $tmp[0] } = undef;

  $fragTypeMF{ $tmp[3] }{ $tmp[1] }{ $tmp[0] } = undef;


  # OECD definition
  if ( $tmp[2] eq "fragment" && $tmp[3] eq "singleton" ) {
    if ( $tmp[1] eq "CF3" ) {
      if ( exists( $oecd{ "CF3" }{ $tmp[0] } ) ) {
        $oecd{ "CF3" }{ $tmp[0] }++;
      } else {
        $oecd{ "CF3" }{ $tmp[0] } = 1;
      }
    } elsif ( $tmp[1] eq "CF2" ) {
      if ( exists( $oecd{ "CF2" }{ $tmp[0] } ) ) {
        $oecd{ "CF2" }{ $tmp[0] }++;
      } else {
        $oecd{ "CF2" }{ $tmp[0] } = 1;
      }
    } else {
      print STDERR ":: WARNING :: unknown singleton fragment type: $tmp[1]\n";
    }
  } else {
    if ( $tmp[2] eq "fragment" || $tmp[2] eq "molecule") {
      my $t3 = $tmp[3];
      if ( $t3 =~ /molecule-/ ) { $t3 =~ s/molecule\-//; }

      my $l = join( " ", $tmp[7], $tmp[8], $t3 );
      if ( exists( $oecd{ "larger" }{ $tmp[0] }{ $l } ) ) {
        $oecd{ "larger" }{ $tmp[0] }{ $l }++;
      } else {
        $oecd{ "larger" }{ $tmp[0] }{ $l } = 1;
      }

      if ( exists( $of{ $tmp[0] } ) ) {
        $of{ $tmp[0] }++;
      } else {
        $of{ $tmp[0] } = 1;
      }
#    } elsif ( $tmp[2] eq "molecule" ) {
#      if ( exists( $oecd{ "Perfluorinated PFAS" }{ $tmp[0] } ) ) {
#        print STDERR ":: WARNING :: multiple defintions of whole molecule for CID: $tmp[0]\n";
#      } else {
#        $oecd{ "Perfluorinated PFAS" }{ $tmp[0] } = 1;
#      } 
    } else {
      print STDERR ":: WARNING :: unknown PFAS type: $tmp[2]\n";
    }
  }



  # Separate out molecule cases
#  if ( $tmp[2] eq "molecule" ) {
#    
#    $mmf{ $tmp[1] }{ $tmp[0] } = undef;
#    next;
#  }

  # Track unique CIDs
  if ( exists( $c{ $tmp[0] } ) ) {
    $c{ $tmp[0] }++;
  } else {
    $c{ $tmp[0] } = 1;
  }

  # Track unique fragment type
  if ( exists( $t{ $tmp[3] } ) ) {
    $t{ $tmp[3] }++;
  } else {
    $t{ $tmp[3] } = 1;
  }

  # Track trio of MF, FragType, CID
  $mf{ $tmp[1] }{ $tmp[3] }{ $tmp[0] } = undef; 

  # CID and then MF w/ type
#  my $k = join( "-", $tmp[1], $tmp[3] );
#print STDERR "$tmp[3]\n";
  my $k = join( " ", $tmp[7], $tmp[8], $tmp[3] );
  if ( exists( $cmf{ $tmp[0] }{ $k } ) ) {
    $cmf{ $tmp[0] }{ $k }++;
  } else {
    $cmf{ $tmp[0] }{ $k } = 1; 
  }
}


# OECD PFAS Tree
my %oecdc = ();
##$oecdc{ "Perfluorinated PFAS" } = 0;
$oecdc{ "Molecule contains PFAS parts larger than CF2/CF3" } = 0;
$oecdc{ "Contains isolated CF3" } = 0;
$oecdc{ "Contains isolated CF2" } = 0;
$oecdc{ "Contains only isolated CF2/CF3" } = 0;
$oecdc{ "Contains only isolated CF2" } = 0;
$oecdc{ "Contains only isolated CF3" } = 0;
$oecdc{ "Contains CF2 and larger PFAS parts" } = 0;
$oecdc{ "Contains CF3 and larger PFAS parts" } = 0;


open( UMF, "| gzip > treeoecd.tsv.gz" ) || die "Unable to write: treeoecd.tsv\n";
#foreach my $cid ( keys( %{ $oecd{ "PFAS molecule" } } ) ) {
#  if ( exists( $oecd{ "PFAS fragment" }{ $cid } ) ||
#       exists( $oecd{ "Contains CF3 fragment" }{ $cid } ) ||
#       exists( $oecd{ "Contains CF2 fragment" }{ $cid } ) ) {
#    print STDERR ":: WARNING :: PFAS Molecule is also a fragment for CID: $cid\n";
#  } else {
#    print UMF "OECD PFAS definition\tPFAS molecule\t$cid\n";
#    $oecdc{ "PFAS molecule" }++;
#  }
#}
my %oecd_part = ();
my %oecd_type = ();
foreach my $cid ( keys( %{ $oecd{ "larger" } } ) ) {
  my $ncf2 = 0;
  if ( exists( $oecd{ "CF2" }{ $cid } ) ) {
    $ncf2 = $oecd{ "CF2" }{ $cid };
  }
  my $ncf3 = 0;
  if ( exists( $oecd{ "CF3" }{ $cid } ) ) {
    $ncf3 = $oecd{ "CF3" }{ $cid };
  }

  my %uft = ();  # unique fragment types
  foreach my $lt ( keys( %{ $oecd{ "larger" }{ $cid } } ) ) {
    my ( $nc, $nf, $t ) = split( /\s/, $lt, 3 );
    $uft{ $t } = undef;
  }

  # Generate the MF bin
  my $frag_count = "Contains ";
  if ( $ncf2 != 0 ) {
    $frag_count .= sprintf( "%02dxCF2,", $ncf2 );
  }
  if ( $ncf3 != 0 ) {
    $frag_count .= sprintf( "%02dxCF3,", $ncf3 );
  }

  my @lt = ();
  foreach my $lt ( keys( %{ $oecd{ "larger" }{ $cid } } ) ) {
    my $nmf = $oecd{ "larger" }{ $cid }{ $lt };
    my ( $nc, $nf, $t ) = split( /\s/, $lt, 3 );
    push( @lt, sprintf( "%02dxC%02dF%02d-%s", $nmf, $nc, $nf, $t ) );
  }
  $frag_count .= join( ",", sort( @lt ) );


  # Generate the Frag Count based break down
  my $n = $c{ $cid };  # Total count of fragments
  my $l = sprintf( "Contains %02d isolated PFAS parts", $n );
  if ( $n == 1 ) {
    chop( $l );
  } elsif ( $n > 15 ) {
    $l = "Contains >15 isolated PFAS parts";
  }

  $oecd_part{ $l }{ $frag_count }{ $cid } = undef;
#  print UMF "OECD PFAS definition\tMolecule contains PFAS parts larger than CF2/CF3\tBreakdown by isolated PFAS part count\t$l\t$frag_count\t$cid\n";

  # Generate the Frag Type based break down
  foreach my $t ( keys( %uft ) ) {
    $oecd_type{ $t }{ $frag_count }{ $cid } = undef;
#    print UMF "OECD PFAS definition\tMolecule contains PFAS parts larger than CF2/CF3\tBreakdown by isolated PFAS part type\tContains isolated $t PFAS part\t$frag_count\t$cid\n";
  }

  $oecdc{ "Molecule contains PFAS parts larger than CF2/CF3" }++;
}

# Determine counts for larger PFAS parts
foreach my $l ( keys( %oecd_part ) ) {
  my @fc = keys( %{ $oecd_part{ $l } } );
  my $nfc = @fc;  # Count ot count-MF-type trios in this section

  my $nt = 0;  # Count of CIDs in this section
  foreach my $frag_count ( @fc ) {
    $nt += keys( %{ $oecd_part{ $l }{ $frag_count } } );
  }

  if ( $nt > 2500 && $nfc > 20 ) {  # Must be more than 2500 CIDs .and. must have more than 20 fragment count-MF-types
    my %nfcc = ();
    my %nfct = ();
    foreach my $frag_count ( @fc ) {
      my @cid = keys( %{ $oecd_part{ $l }{ $frag_count } } );
      my $n = @cid;

      my $nl = "";
      if ( $n > 100000 ) {
        $nl = "Count of molecules >100000";
      } elsif ( $n > 10000 ) {
        $nl = "Count of molecules 10001-100000";
      } elsif ( $n > 1000 ) {
        $nl = "Count of molecules 01001-10000";
      } elsif ( $n > 100 ) {
        $nl = "Count of molecules 00101-1000";
      } elsif ( $n > 10 ) {
        $nl = "Count of molecules 00011-100";
      } else {
        $nl = "Count of molecules 00001-10";
      }

      if ( exists( $nfcc{ $nl } ) ) {
        $nfcc{ $nl }++;
      } else {
        $nfcc{ $nl } = 1;
      }

      my @frags = split( /\,/, $frag_count );
      foreach my $frag ( @frags ) {
        if ( ! ( $frag =~ /\-/ ) ) { next; }
        my ( $cmf, $type ) = split( /\-/, $frag, 2 );
        $nfct{ $nl }{ $type } = undef;
      }
    }

    foreach my $frag_count ( @fc ) {
      # Remove special case of CF2 diradical
      if ( $frag_count eq "Contains 01xC01F02-singleton" ) { next; }

      my @cid = keys( %{ $oecd_part{ $l }{ $frag_count } } );
      my $n = @cid;

      my $nl = "";
      if ( $n > 100000 ) {
        $nl = "Count of molecules >100000";
      } elsif ( $n > 10000 ) {
        $nl = "Count of molecules 10001-100000";
      } elsif ( $n > 1000 ) {
        $nl = "Count of molecules 01001-10000";
      } elsif ( $n > 100 ) {
        $nl = "Count of molecules 00101-1000";
      } elsif ( $n > 10 ) {
        $nl = "Count of molecules 00011-100";
      } else {
        $nl = "Count of molecules 00001-10";
      }

      my $nuft = keys( %{ $nfct{ $nl } } );
      if ( $nuft == 1 || $nfcc{ $nl } <= 20 ) {  # Suppress adding type as only one or too few variations to breakdown more
        foreach my $cid ( @cid ) {
          print UMF "OECD PFAS definition\tMolecule contains PFAS parts larger than CF2/CF3\tBreakdown by isolated PFAS part count\t$l\t$nl\t$frag_count\t$cid\n";
        }
      } else {
        my %uft = ();
        my @frags = split( /\,/, $frag_count );
        foreach my $frag ( @frags ) {
          if ( ! ( $frag =~ /\-/ ) ) { next; }
          my ( $cmf, $type ) = split( /\-/, $frag, 2 );
          if ( exists( $uft{ $type } ) ) { next; }

          $uft{ $type } = undef;
          foreach my $cid ( @cid ) {
            print UMF "OECD PFAS definition\tMolecule contains PFAS parts larger than CF2/CF3\tBreakdown by isolated PFAS part count\t$l\t$nl\tContains isolated $type PFAS part\t$frag_count\t$cid\n";
          }
        }
      }
    }
  } elsif ( $nfc > 20 ) {  # More than 20 trios of count-MF-type .. so breakdown further
    my %nfct = ();
    foreach my $frag_count ( @fc ) {
      my @cid = keys( %{ $oecd_part{ $l }{ $frag_count } } );
      my $n = @cid;

      my @frags = split( /\,/, $frag_count );
      foreach my $frag ( @frags ) {
        if ( ! ( $frag =~ /\-/ ) ) { next; }
        my ( $cmf, $type ) = split( /\-/, $frag, 2 );
        $nfct{ $type } = undef;
      }
    }

    my $nuft = keys( %nfct );
    foreach my $frag_count ( @fc ) {
      # Remove special case of CF2 diradical
      if ( $frag_count eq "Contains 01xC01F02-singleton" ) { next; }

      my @cid = keys( %{ $oecd_part{ $l }{ $frag_count } } );

      if ( $nuft == 1 ) {  # Suppress adding type as only one, so pointless to do so but annoy the user
        foreach my $cid ( @cid ) {
          print UMF "OECD PFAS definition\tMolecule contains PFAS parts larger than CF2/CF3\tBreakdown by isolated PFAS part count\t$l\t$frag_count\t$cid\n";
        }
      } else {
        my %uft = ();  # unique fragment types
        my @frags = split( /\,/, $frag_count );
        foreach my $frag ( @frags ) {
          if ( ! ( $frag =~ /\-/ ) ) { next; }
          my ( $cmf, $type ) = split( /\-/, $frag, 2 );
          if ( exists( $uft{ $type } ) ) { next; }
          $uft{ $type } = undef;

          foreach my $cid ( @cid ) {
            print UMF "OECD PFAS definition\tMolecule contains PFAS parts larger than CF2/CF3\tBreakdown by isolated PFAS part count\t$l\tContains isolated $type PFAS part\t$frag_count\t$cid\n";
          }
        }
      }
    }
  } else {
    foreach my $frag_count ( @fc ) {
      # Remove special case of CF2 diradical
      if ( $frag_count eq "Contains 01xC01F02-singleton" ) { next; }

      my @cid = keys( %{ $oecd_part{ $l }{ $frag_count } } );
      foreach my $cid ( @cid ) {
        print UMF "OECD PFAS definition\tMolecule contains PFAS parts larger than CF2/CF3\tBreakdown by isolated PFAS part count\t$l\t$frag_count\t$cid\n";
      }
    }
  }
}

# Determine counts for larger PFAS types
foreach my $l ( keys( %oecd_type ) ) {
  my @fc = keys( %{ $oecd_type{ $l } } );
  my $nfc = @fc;  # Count ot count-MF-type trios in this section


  my $nt = 0;
  foreach my $frag_count ( keys( %{ $oecd_type{ $l } } ) ) {
    $nt += keys( %{ $oecd_type{ $l }{ $frag_count } } );
  }

  if ( $nt > 1000 ) {  # Breakdown further w/ 'Also contains PFAS type'
    foreach my $frag_count ( keys( %{ $oecd_type{ $l } } ) ) {
      # Remove special case of CF2 diradical
      if ( $l eq "singleton" && $frag_count eq "Contains 01xC01F02-singleton" ) { next; }

      my @cid = keys( %{ $oecd_type{ $l }{ $frag_count } } );
      my $n = @cid;

      # Get the CID count grouping, if needed
      my $nl = "";
      if ( $nt > 100000 ) {
        if ( $n > 100000 ) {
          $nl = "\tCount of molecules >100000";
        } elsif ( $n > 10000 ) {
          $nl = "\tCount of molecules 10001-100000";
        } elsif ( $n > 1000 ) {
          $nl = "\tCount of molecules 01001-10000";
        } elsif ( $n > 100 ) {
          $nl = "\tCount of molecules 00101-1000";
        } elsif ( $n > 10 ) {
          $nl = "\tCount of molecules 00011-100";
        } else {
          $nl = "\tCount of molecules 00001-10";
        }
      }

      # Determine the unique count of types
      my %uft = ();  # unique fragment types
      my @frags = split( /\,/, $frag_count );
      foreach my $frag ( @frags ) {
        if ( ! ( $frag =~ /\-/ ) ) { next; }

        my ( $cmf, $type ) = split( /\-/, $frag, 2 );
        $uft{ $type } = undef;
      }

      my $ng = "";
      my $ntype = keys( %uft );
      if ( $ntype == 1 ) {  # Just the current type .. so do nothing special
        my @type = keys( %uft );
        my $type = $type[0];
        foreach my $cid ( @cid ) {
          if ( $nl ne "" && $n <= 100 ) {
            my $nf = $c{ $cid };
            $ng = sprintf( "\tContains %02d isolated PFAS parts", $nf );
            if ( $nf == 1 ) {
              chop( $ng );
            } elsif ( $nf > 15 ) {
              $ng = "\tContains >15 isolated PFAS parts";
            }
          }

          print UMF "OECD PFAS definition\tMolecule contains PFAS parts larger than CF2/CF3\tBreakdown by isolated PFAS part type\tContains isolated $l PFAS part$nl$ng\tOnly contains isolated $type PFAS part\t$frag_count\t$cid\n";
        }
      } else {
        %uft = ();
        $uft{ $l } = undef;  # Current type
        
        foreach my $frag ( @frags ) {
          if ( ! ( $frag =~ /\-/ ) ) { next; }

          my ( $cmf, $type ) = split( /\-/, $frag, 2 );
          if ( exists( $uft{ $type } ) ) { next; }

          $uft{ $type } = undef;
          foreach my $cid ( @cid ) {
            if ( $nl ne "" && $n <= 100 ) {
              my $nf = $c{ $cid };
              $ng = sprintf( "\tContains %02d isolated PFAS parts", $nf );
              if ( $nf == 1 ) {
                chop( $ng );
              } elsif ( $nf > 15 ) {
                $ng = "\tContains >15 isolated PFAS parts";
              }
            }

            print UMF "OECD PFAS definition\tMolecule contains PFAS parts larger than CF2/CF3\tBreakdown by isolated PFAS part type\tContains isolated $l PFAS part$nl$ng\tAlso contains isolated $type PFAS part\t$frag_count\t$cid\n";
          }
        }
      }
    }
  } elsif ( $l eq "singleton" ) {
    foreach my $frag_count ( keys( %{ $oecd_type{ $l } } ) ) {
      # Remove special case of CF2 diradical
      if ( $l eq "singleton" && $frag_count eq "Contains 01xC01F02-singleton" ) { next; }

      my @cid = keys( %{ $oecd_type{ $l }{ $frag_count } } );
      foreach my $cid ( @cid ) {
        print UMF "OECD PFAS definition\tMolecule contains PFAS parts larger than CF2/CF3\tBreakdown by isolated PFAS part type\tContains isolated $l PFAS part\t$frag_count\t$cid\n";
      }
    }
  } else {  # Do nothing special
    foreach my $frag_count ( keys( %{ $oecd_type{ $l } } ) ) {
      # Remove special case of CF2 diradical
      if ( $l eq "singleton" && $frag_count eq "Contains 01xC01F02-singleton" ) { next; }

      my @cid = keys( %{ $oecd_type{ $l }{ $frag_count } } );
      foreach my $cid ( @cid ) {
        print UMF "OECD PFAS definition\tMolecule contains PFAS parts larger than CF2/CF3\tBreakdown by isolated PFAS part type\tContains isolated $l PFAS part\t$frag_count\t$cid\n";
      }
    }
  }
}


my %oecd_cf2 = ();
foreach my $cid ( keys( %{ $oecd{ "CF2" } } ) ) {
  $oecdc{ "Contains isolated CF2" }++;
  if ( exists( $oecd{ "larger" }{ $cid } ) ) {
    # Get the fragment counts by type
    my $ncf2 = $oecd{ "CF2" }{ $cid };
    my $frag_count = sprintf( "Contains %02dxCF2", $ncf2 );

    my $ncf3 = 0;
    if ( exists( $oecd{ "CF3" }{ $cid } ) ) {
      $ncf3 = $oecd{ "CF3" }{ $cid };
      $frag_count .= sprintf( ",%02dxCF3", $ncf3 );
    }

    my @lt = ();
    foreach my $lt ( keys( %{ $oecd{ "larger" }{ $cid } } ) ) {
      my $nmf = $oecd{ "larger" }{ $cid }{ $lt };
      my ( $nc, $nf, $t ) = split( /\s/, $lt, 3 );
      push( @lt, sprintf( ",%02dxC%02dF%02d-%s", $nmf, $nc, $nf, $t ) );
    }
    $frag_count .= join( "", sort( @lt ) );

    $oecd_cf2{ $frag_count }{ $cid } = undef;
#    print UMF "OECD PFAS definition\tMolecule contains isolated CF2\tContains CF2 and larger PFAS parts\t$frag_count\t$cid\n";
    $oecdc{ "Contains CF2 and larger PFAS parts" }++;
  } elsif ( exists( $oecd{ "CF3" }{ $cid } ) ) {
    my $ncf2 = $oecd{ "CF2" }{ $cid };
    my $ncf3 = $oecd{ "CF3" }{ $cid };
    my $frag_count = sprintf( "Contains %02dxCF2,%02dxCF3", $ncf2, $ncf3 );
    print UMF "OECD PFAS definition\tMolecule contains isolated CF2\tContains only isolated CF2/CF3\t$frag_count\t$cid\n";
    $oecdc{ "Contains only isolated CF2/CF3" }++;
  } else {
    my $n = $oecd{ "CF2" }{ $cid };
    my $frag_count = sprintf( "Contains %02dxCF2", $n );
    print UMF "OECD PFAS definition\tMolecule contains isolated CF2\tContains only isolated CF2\t$frag_count\t$cid\n";
    $oecdc{ "Contains only isolated CF2" }++;
  }
}

# Determine counts for larger PFAS part types
my %cf2_st = ();  # Count the trios of count-MF-type in the case of <=10 CIDs
foreach my $frag_count ( keys( %oecd_cf2 ) ) {
  my @cid = keys( %{ $oecd_cf2{ $frag_count } } );
  my $n = @cid;
#  if ( $n <= 10 ) {
    my %uft = ();  # unique fragment types
    my @frags = split( /\,/, $frag_count );
    foreach my $frag ( @frags ) {
      if ( ! ( $frag =~ /\-/ ) ) {
#        print STDERR "Skipped type: $frag for entity: $frag_count\n";
        next;
      }
      my ( $cmf, $type ) = split( /\-/, $frag, 2 );
      if ( exists( $uft{ $type } ) ) { next; }  # Only count type once per set of trio of count-MF-type

      $uft{ $type } = undef;
      if ( exists( $cf2_st{ $type } ) ) {
        $cf2_st{ $type }++;
      } else {
        $cf2_st{ $type } = 1;
      }
    }
#  }
}
foreach my $frag_count ( keys( %oecd_cf2 ) ) {
  my @cid = keys( %{ $oecd_cf2{ $frag_count } } );
  my $n = @cid;

  # Get the CID count grouping, if needed
#  my $nl = "";
#  if ( $n > 100000 ) {
#    if ( $n > 100000 ) {
#      $nl = "\tCount of molecules >100000";
#    } elsif ( $n > 10000 ) {
#      $nl = "\tCount of molecules 10001-100000";
#    } elsif ( $n > 1000 ) {
#      $nl = "\tCount of molecules 01001-10000";
#    } elsif ( $n > 100 ) {
#      $nl = "\tCount of molecules 00101-1000";
#    } elsif ( $n > 10 ) {
#      $nl = "\tCount of molecules 00011-100";
#    } else {
#      $nl = "\tCount of molecules 00001-10";
#    }
#  }
#
#  if ( $n > 100 ) {
#    if ( $n > 100000 ) {
#      foreach my $cid ( @cid ) {
#        print UMF "OECD PFAS definition\tMolecule contains isolated CF2\tContains CF2 and larger PFAS parts\t$nl\t$frag_count\t$cid\n";
#      }
#    } else {
#      foreach my $cid ( @cid ) {
#        print UMF "OECD PFAS definition\tMolecule contains isolated CF2\tContains CF2 and larger PFAS parts\t$frag_count\t$cid\n";
#      }
#    }
#  } else {
    my %uft = ();  # unique fragment types
    my @frags = split( /\,/, $frag_count );
    foreach my $frag ( @frags ) {
      if ( ! ( $frag =~ /\-/ ) ) {
#        print STDERR "Skipped type: $frag for entity: $frag_count\n";
        next;
      }
      my ( $cmf, $type ) = split( /\-/, $frag, 2 );
      if ( exists( $uft{ $type } ) ) { next; }
      $uft{ $type } = undef;

      if ( $cf2_st{ $type } > 20 ) {
        foreach my $cid ( @cid ) {
          my $nf = $c{ $cid };  # Total count of fragments
          my $l = sprintf( "Contains %02d isolated PFAS parts", $nf );
          if ( $nf == 1 ) {
            chop( $l );
          } elsif ( $nf > 15 ) {
            $l = "Contains >15 isolated PFAS parts";
          }

          print UMF "OECD PFAS definition\tMolecule contains isolated CF2\tContains CF2 and larger PFAS parts\tContains isolated $type PFAS part\t$l\t$frag_count\t$cid\n";
        }
      } else {
        foreach my $cid ( @cid ) {
          print UMF "OECD PFAS definition\tMolecule contains isolated CF2\tContains CF2 and larger PFAS parts\tContains isolated $type PFAS part\t$frag_count\t$cid\n";
        }
      }
    }
#  }
}

my %oecd_cf3 = ();
foreach my $cid ( keys( %{ $oecd{ "CF3" } } ) ) {
  $oecdc{ "Contains isolated CF3" }++;
  if ( exists( $oecd{ "larger" }{ $cid } ) ) {
    # Get the fragment counts by type
    my $frag_count = "Contains ";
    my $ncf2 = 0;
    if ( exists( $oecd{ "CF2" }{ $cid } ) ) {
      $ncf2 = $oecd{ "CF2" }{ $cid };
      $frag_count .= sprintf( "%02dxCF2,", $ncf2 );
    }

    my $ncf3 = $oecd{ "CF3" }{ $cid };
    $frag_count .= sprintf( "%02dxCF3", $ncf3 );

    my @lt = ();
    foreach my $lt ( sort( keys( %{ $oecd{ "larger" }{ $cid } } ) ) ) {
      my $nmf = $oecd{ "larger" }{ $cid }{ $lt };
      my ( $nc, $nf, $t ) = split( /\s/, $lt, 3 );
      push( @lt, sprintf( ",%02dxC%02dF%02d-%s", $nmf, $nc, $nf, $t ) );
    }
    $frag_count .= join( "", sort( @lt ) );

    $oecd_cf3{ $frag_count }{ $cid } = undef;
#    print UMF "OECD PFAS definition\tMolecule contains isolated CF3\tContains CF3 and larger PFAS parts\t$frag_count\t$cid\n";
    $oecdc{ "Contains CF3 and larger PFAS parts" }++;
  } elsif ( exists( $oecd{ "CF2" }{ $cid } ) ) {
    my $ncf2 = $oecd{ "CF2" }{ $cid };
    my $ncf3 = $oecd{ "CF3" }{ $cid };
    my $frag_count = sprintf( "Contains %02dxCF2,%02dxCF3", $ncf2, $ncf3 );
    print UMF "OECD PFAS definition\tMolecule contains isolated CF3\tContains only isolated CF2/CF3\t$frag_count\t$cid\n";
    $oecdc{ "Contains only isolated CF2/CF3" }++;
  } else {
    my $n = $oecd{ "CF3" }{ $cid };
    my $frag_count = sprintf( "Contains %02dxCF3", $n );
    print UMF "OECD PFAS definition\tMolecule contains isolated CF3\tContains only isolated CF3\t$frag_count\t$cid\n";
    $oecdc{ "Contains only isolated CF3" }++;
  }
}

# Determine counts for larger PFAS part types
my %cf3_st = ();  # Count the trios of count-MF-type in the case of <=10 CIDs
foreach my $frag_count ( keys( %oecd_cf3 ) ) {
  my @cid = keys( %{ $oecd_cf3{ $frag_count } } );
  my $n = @cid;
#  if ( $n <= 10 ) {
    my %uft = ();  # unique fragment types
    my @frags = split( /\,/, $frag_count );
    foreach my $frag ( @frags ) {
      if ( ! ( $frag =~ /\-/ ) ) {
#        print STDERR "Skipped type: $frag for entity: $frag_count\n";
        next;
      }
      my ( $cmf, $type ) = split( /\-/, $frag, 2 );
      if ( exists( $uft{ $type } ) ) { next; }  # Only count type once per set of trio of count-MF-type

      $uft{ $type } = undef;
      if ( exists( $cf3_st{ $type } ) ) {
        $cf3_st{ $type }++;
      } else {
        $cf3_st{ $type } = 1;
      }
    }
#  }
}
foreach my $frag_count ( keys( %oecd_cf3 ) ) {
  my @cid = keys( %{ $oecd_cf3{ $frag_count } } );
  my $n = @cid;

  # Get the CID count grouping, if needed
#  my $nl = "";
#  if ( $n > 100000 ) {
#    if ( $n > 100000 ) {
#      $nl = "\tCount of molecules >100000";
#    } elsif ( $n > 10000 ) {
#      $nl = "\tCount of molecules 10001-100000";
#    } elsif ( $n > 1000 ) {
#      $nl = "\tCount of molecules 01001-10000";
#    } elsif ( $n > 100 ) {
#      $nl = "\tCount of molecules 00101-1000";
#    } elsif ( $n > 10 ) {
#      $nl = "\tCount of molecules 00011-100";
#    } else {
#      $nl = "\tCount of molecules 00001-10";
#    }
#  }
#
#  if ( $n > 100 ) {
#    foreach my $cid ( @cid ) {
#      print UMF "OECD PFAS definition\tMolecule contains isolated CF3\tContains CF3 and larger PFAS parts\t$nl\t$frag_count\t$cid\n";
#    }
#  } else {
    my %uft = ();  # unique fragment types
    my @frags = split( /\,/, $frag_count );
    foreach my $frag ( @frags ) {
      if ( ! ( $frag =~ /\-/ ) ) {
#        print STDERR "Skipped type: $frag for entity: $frag_count\n";
        next;
      }
      my ( $cmf, $type ) = split( /\-/, $frag, 2 );
      if ( exists( $uft{ $type } ) ) { next; }
      $uft{ $type } = undef;

      if ( $cf3_st{ $type } > 20 ) {
        foreach my $cid ( @cid ) {
          my $nf = $c{ $cid };  # Total count of fragments
          my $l = sprintf( "Contains %02d isolated PFAS parts", $nf );
          if ( $nf == 1 ) {
            chop( $l );
          } elsif ( $nf > 15 ) {
            $l = "Contains >15 isolated PFAS parts";
          }

          print UMF "OECD PFAS definition\tMolecule contains isolated CF3\tContains CF3 and larger PFAS parts\tContains isolated $type PFAS part\t$l\t$frag_count\t$cid\n";
        }
      } else {
        foreach my $cid ( @cid ) {
          print UMF "OECD PFAS definition\tMolecule contains isolated CF3\tContains CF3 and larger PFAS parts\tContains isolated $type PFAS part\t$frag_count\t$cid\n";
        }
      }
    }
#  }
}

close( TSV );
close( UMF );

foreach my $l ( sort( keys( %oecdc ) ) ) {
  print STDERR "$l\t$oecdc{$l}\n";
}


sub bynum { $a<=>$b };

