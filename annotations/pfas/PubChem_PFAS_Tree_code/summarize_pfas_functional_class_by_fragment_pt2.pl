#!/usr/bin/perl

use strict;
use warnings;


# Parameters
my $min_cid = 10;  # Minimum CID count to expand out by MF
my $max_node = 25;  # Maximum count of node subnodes
my $max_node_min_cid = 500;  # In case max node exceeded, still show nodes with CID counts beyond a particular size
my $print_cid = 1;  # Print CID (1) or Count (0)?

# Constants
my $tlabel = "PFAS breakdowns by chemistry";  # Top label of the tree


# Read in commandline info
my $file = $ARGV[0];
if ( ! defined( $file ) || $file eq "" ) {
  die "Please specify a gz compressed file to operate on\n";
}
print STDERR "Reading file: $file\n";


my %c = ();  # Primary data container
open( TSV, "gunzip < $file |" ) || die "Unable to read file: $file\n";
while ( $_ = <TSV> ) {
  chop;
  my @tmp = split( /Breakdown by /, $_ );
  my $ntmp = @tmp;

  if ( $ntmp == 3 ) {
    my @tmp2 = split( /	/, $tmp[2] );
    my $cid = pop( @tmp2 );
    my $tmp2 = join( "	", @tmp2 );
    chop( $tmp[1] );  # Remove tab before second "Breakdown by"
    $c{ $tmp[1] }{ $tmp2 }{ $cid } = undef;

#if ( $tmp[1] =~ /C10F21/ ) {
#  print STDERR "tmp1: \"$tmp[1]\"  tmp2: \"$tmp[2]\"  cid: \"$cid\"\n";
#} elsif ( $tmp2 =~ /C10F21/ ) {
#  print STDERR "tmp1: \"$tmp[1]\"  tmp2: \"$tmp2\"  cid: \"$cid\"\n";
#}

  } else {
    print STDERR ":: WARNING :: Unrecognized count of \"Breakdown by \": $ntmp :: $_\n";
  }
}
close( TSV );


# Examine the upper nodes
my %c2 = ();
my %c3 = ();
my %c4 = ();
#my %et = ();
foreach my $t1 ( sort( keys( %c ) ) ) {
  my @t3 = split( /	/, $t1 );
  my $nt3 = @t3;


  my @t2 = keys( %{ $c{ $t1 } } );
  if ( $nt3 == 2 ) {
    foreach my $t2 ( @t2 ) {
      foreach my $cid ( keys( %{ $c{ $t1 }{ $t2 } } ) ) {
        $c2{ $t3[0] }{ $t3[1] }{ $t2 }{ $cid } = undef;
      }
    }
  } elsif ( $nt3 == 3 ) {
    foreach my $t2 ( @t2 ) {
      foreach my $cid ( keys( %{ $c{ $t1 }{ $t2 } } ) ) {
        $c3{ $t3[0] }{ $t3[1] }{ $t3[2] }{ $t2 }{ $cid } = undef;
      }
    }
  } elsif ( $nt3 == 4 ) {
    if ( $t3[2] =~ /ethers/ ) {
      foreach my $t2 ( @t2 ) {
        foreach my $cid ( keys( %{ $c{ $t1 }{ $t2 } } ) ) {
          $c2{ $t3[0] }{ $t3[1] }{ $t2 }{ $cid } = undef;
#          $et{ $t3[0] }{ $t3[1] }{ $t3[2] }{ $t3[3] }{ $cid } = undef;
        }
      }
    } else {  # Handle ethers differently
      foreach my $t2 ( @t2 ) {
        foreach my $cid ( keys( %{ $c{ $t1 }{ $t2 } } ) ) {
          $c4{ $t3[0] }{ $t3[1] }{ $t3[2] }{ $t3[3] }{ $t2 }{ $cid } = undef;
        }
      }
    }
  } else {
    print STDERR ":: WARNING :: Skipped unexpected depth of $nt3 :: $t1\n";
  }
}


# Check first case
my @c2 = sort( keys( %c2 ) );
my $nc2 = @c2;
if ( $nc2 > $max_node ) { print STDERR ":: WARNING :: Case of C2 1st w/ $nc2\n"; }

foreach my $tc21 ( @c2 ) {
  my @tc22 = sort( keys( %{ $c2{ $tc21 } } ) );
  my $ntc22 = @tc22;

  # Check for special cases
  my $ltc21 = $tc21;
  my $sc_tc21 = 0;
  if ( $tc21 =~ /More / ) {
    $sc_tc21 = 1;
    $ltc21 =~ s/More //;  # Yet even more ..
  } elsif ( $tc21 =~ /Yet more / ) {
    $sc_tc21 = 1;
    $ltc21 =~ s/Yet more //;  # Yet even more ..
  } elsif ( $tc21 =~ /Contains / ) {
    $ltc21 =~ s/Contains /contains /;  # Yet more contains ..
  }

  if ( $ntc22 <= $max_node ) {
    foreach my $tc22 ( @tc22 ) {
      my $n22 = 0;
      foreach my $ln ( keys( %{ $c2{ $tc21 }{ $tc22 } } ) ) {  # Loop over lower nodes
        my $n = keys( %{ $c2{ $tc21 }{ $tc22 }{ $ln } } );  # CID count
        $n22 += $n;
      }

      if ( $n22 < $min_cid ) {  # Too few CIDs .. truncate lower nodes
        if ( $print_cid == 0 ) {
          print "2 :t: $n22\t$tlabel\tBreakdown by $tc21\t$tc22\n";
        } else {
          foreach my $ln ( sort( keys( %{ $c2{ $tc21 }{ $tc22 } } ) ) ) {  # Loop over lower nodes
            foreach my $cid ( sort bynum( keys( %{ $c2{ $tc21 }{ $tc22 }{ $ln } } ) ) ) {
              print "$tlabel\tBreakdown by $tc21\t$tc22\t$cid\n";
            }
          }
        }
      } else {
        if ( $print_cid == 0 ) {
          foreach my $ln ( sort( keys( %{ $c2{ $tc21 }{ $tc22 } } ) ) ) {  # Loop over lower nodes
            my $n = keys( %{ $c2{ $tc21 }{ $tc22 }{ $ln } } );  # CID count
            print "2 :: $n\t$tlabel\tBreakdown by $tc21\t$tc22\tBreakdown by $ln\n";
          }
        } else {
          foreach my $ln ( sort( keys( %{ $c2{ $tc21 }{ $tc22 } } ) ) ) {  # Loop over lower nodes
            foreach my $cid ( sort bynum( keys( %{ $c2{ $tc21 }{ $tc22 }{ $ln } } ) ) ) {
              print "$tlabel\tBreakdown by $tc21\t$tc22\tBreakdown by $ln\t$cid\n";
            }
          }
        }
      }
    }
  } else {
    # Determine size of this node
    my %n = ();
    foreach my $tc22 ( @tc22 ) {
      my $n22 = 0;
      foreach my $ln ( keys( %{ $c2{ $tc21 }{ $tc22 } } ) ) {  # Loop over lower nodes
        my $n = keys( %{ $c2{ $tc21 }{ $tc22 }{ $ln } } );  # CID count
        $n22 += $n;
      }
      $n{ $n22 }{ $tc22 } = undef;
    }

    my $iym = 0;  # Track yet more cases to avoid too many nodes
    my $i1 = 0;  # Track count of cases
    my $i2 = 0;  # Track 'pages' of cases
    foreach my $n22 ( sort bynum( keys( %n ) ) ) {
      foreach my $tc22 ( sort( keys( %{ $n{ $n22 } } ) ) ) {
        $i1++;  # Track count of cases
        if ( ( $i1 % $max_node ) == 0 ) { $i2++; }  # Track 'pages' of cases

        if ( $i2 != 0 ) {
          if ( $n22 < $max_node_min_cid ||
               $iym > $max_node ) {
            if ( $print_cid == 0 ) {
              foreach my $ln ( sort( keys( %{ $c2{ $tc21 }{ $tc22 } } ) ) ) {  # Loop over lower nodes
                my $n = keys( %{ $c2{ $tc21 }{ $tc22 }{ $ln } } );  # CID count
                if ( $sc_tc21 == 1 ) {
                  print "2 :y: $n\t$tlabel\tBreakdown by $tc21\tYet even more $ltc21\tBreakdown by $ln\n";
                } else {
                  print "2 :y: $n\t$tlabel\tBreakdown by $tc21\tYet more $ltc21\tBreakdown by $ln\n";
                }
              }
            } else {
              foreach my $ln ( sort( keys( %{ $c2{ $tc21 }{ $tc22 } } ) ) ) {  # Loop over lower nodes
                foreach my $cid ( sort bynum( keys( %{ $c2{ $tc21 }{ $tc22 }{ $ln } } ) ) ) {
#                  print "$tlabel\tBreakdown by $tc21\tYet more $tc21\tBreakdown by $ln\t$cid\n";
                  if ( $sc_tc21 == 1 ) {
                    print "$tlabel\tBreakdown by $tc21\tYet even more $ltc21\tBreakdown by $ln\t$cid\n";
                  } else {
                    print "$tlabel\tBreakdown by $tc21\tYet more $ltc21\tBreakdown by $ln\t$cid\n";
                  }
                }
              }
            }
          } else {
            $iym++;
            if ( $print_cid == 0 ) {
              foreach my $ln ( sort( keys( %{ $c2{ $tc21 }{ $tc22 } } ) ) ) {  # Loop over lower nodes
                my $n = keys( %{ $c2{ $tc21 }{ $tc22 }{ $ln } } );  # CID count
#                print "2 :y: $n\t$tlabel\tBreakdown by $tc21\tYet more $tc21\t$tc22\tBreakdown by $ln\n";
                if ( $sc_tc21 == 1 ) {
                  print "2 :y: $n\t$tlabel\tBreakdown by $tc21\tYet even more $ltc21\t$tc22\tBreakdown by $ln\n";
                } else {
                  print "2 :y: $n\t$tlabel\tBreakdown by $tc21\tYet more $ltc21\t$tc22\tBreakdown by $ln\n";
                }
              }
            } else {
              foreach my $ln ( sort( keys( %{ $c2{ $tc21 }{ $tc22 } } ) ) ) {  # Loop over lower nodes
                foreach my $cid ( sort bynum( keys( %{ $c2{ $tc21 }{ $tc22 }{ $ln } } ) ) ) {
#                  print "$tlabel\tBreakdown by $tc21\tYet more $tc21\t$tc22\tBreakdown by $ln\t$cid\n";
                  if ( $sc_tc21 == 1 ) {
                    print "$tlabel\tBreakdown by $tc21\tYet even more $ltc21\t$tc22\tBreakdown by $ln\t$cid\n";
                  } else {
                    print "$tlabel\tBreakdown by $tc21\tYet more $ltc21\t$tc22\tBreakdown by $ln\t$cid\n";
                  }
                }
              }
            }
          }
        } else {
          if ( $print_cid == 0 ) {
            foreach my $ln ( sort( keys( %{ $c2{ $tc21 }{ $tc22 } } ) ) ) {  # Loop over lower nodes
              my $n = keys( %{ $c2{ $tc21 }{ $tc22 }{ $ln } } );  # CID count
              print "2 :: $n\t$tlabel\tBreakdown by $tc21\t$tc22\tBreakdown by $ln\n";
            }
          } else {
            foreach my $ln ( sort( keys( %{ $c2{ $tc21 }{ $tc22 } } ) ) ) {  # Loop over lower nodes
              foreach my $cid ( sort bynum( keys( %{ $c2{ $tc21 }{ $tc22 }{ $ln } } ) ) ) {
                print "$tlabel\tBreakdown by $tc21\t$tc22\tBreakdown by $ln\t$cid\n";
              }
            }
          }
        }
      }
    }
  }
}


# Check second case
my @c3 = sort( keys( %c3 ) );
my $nc3 = @c3;
if ( $nc3 > $max_node ) { print STDERR ":: WARNING :: Case of C3 1st w/ $nc3\n"; }

foreach my $tc31 ( @c3 ) {
  my @tc32 = sort( keys( %{ $c3{ $tc31 } } ) );
  my $ntc32 = @tc32;
  if ( $ntc32 > $max_node ) { print STDERR ":: WARNING :: Case of C3 2nd w/ $ntc32\n"; }

  foreach my $tc32 ( @tc32 ) {
    my @tc33 = sort( keys( %{ $c3{ $tc31 }{ $tc32 } } ) );
    my $ntc33 = @tc33;

    # Check for special cases
    my $ltc32 = $tc32;
    my $sc_tc32 = 0;
    if ( $tc32 =~ /More / ) {
      $sc_tc32 = 1;
      $ltc32 =~ s/More //;  # Yet even more ..
    } elsif ( $tc32 =~ /Yet more / ) {
      $sc_tc32 = 1;
      $ltc32 =~ s/Yet more //;  # Yet even more ..
    } elsif ( $tc32 =~ /Contains / ) {
      $ltc32 =~ s/Contains /contains /;  # Yet more contains ..
    }

    if ( $ntc33 <= $max_node ) {
      foreach my $tc33 ( @tc33 ) {
        my $n33 = 0;
        foreach my $ln ( keys( %{ $c3{ $tc31 }{ $tc32 }{ $tc33 } } ) ) {  # Loop over lower nodes
          my $n = keys( %{ $c3{ $tc31 }{ $tc32 }{ $tc33 }{ $ln } } );  # CID count
          $n33 += $n;
        }

        if ( $n33 < $min_cid ) {  # Too few CIDs .. truncate lower nodes
          if ( $print_cid == 0 ) {
            print "3 :t: $n33\t$tlabel\tBreakdown by $tc31\t$tc32\t$tc33\n";
          } else {
            foreach my $ln ( sort( keys( %{ $c3{ $tc31 }{ $tc32 }{ $tc33 } } ) ) ) {  # Loop over lower nodes
              foreach my $cid ( sort bynum( keys( %{ $c3{ $tc31 }{ $tc32 }{ $tc33 }{ $ln } } ) ) ) {
                print "$tlabel\tBreakdown by $tc31\t$tc32\t$tc33\t$cid\n";
              }
            }
          }
        } else {
          if ( $print_cid == 0 ) {
            foreach my $ln ( sort( keys( %{ $c3{ $tc31 }{ $tc32 }{ $tc33 } } ) ) ) {  # Loop over lower nodes
              my $n = keys( %{ $c3{ $tc31 }{ $tc32 }{ $tc33 }{ $ln } } );  # CID count
              print "3 :: $n\t$tlabel\tBreakdown by $tc31\t$tc32\t$tc33\tBreakdown by $ln\n";
            }
          } else {
            foreach my $ln ( sort( keys( %{ $c3{ $tc31 }{ $tc32 }{ $tc33 } } ) ) ) {  # Loop over lower nodes
              foreach my $cid ( sort bynum( keys( %{ $c3{ $tc31 }{ $tc32 }{ $tc33 }{ $ln } } ) ) ) {
                print "$tlabel\tBreakdown by $tc31\t$tc32\t$tc33\tBreakdown by $ln\t$cid\n";
              }
            }
          }
        }
      }
    } else {
      # Determine size of this node
      my %n = ();
      foreach my $tc33 ( @tc33 ) {
        my $n33 = 0;
        foreach my $ln ( keys( %{ $c3{ $tc31 }{ $tc32 }{ $tc33 } } ) ) {  # Loop over lower nodes
          my $n = keys( %{ $c3{ $tc31 }{ $tc32 }{ $tc33 }{ $ln } } );  # CID count
          $n33 += $n;
        }
        $n{ $n33 }{ $tc33 } = undef;
      }

      my $iym = 0;  # Track yet more cases to avoid too many nodes
      my $i1 = 0;  # Track count of cases
      my $i2 = 0;  # Track 'pages' of cases
      foreach my $n33 ( sort bynum( keys( %n ) ) ) {
        foreach my $tc33 ( sort( keys( %{ $n{ $n33 } } ) ) ) {
          $i1++;  # Track count of cases
          if ( ( $i1 % $max_node ) == 0 ) { $i2++; }  # Track 'pages' of cases

          if ( $i2 != 0 ) {
            if ( $n33 < $max_node_min_cid ||
                 $iym > $max_node ) {
              if ( $print_cid == 0 ) {
                foreach my $ln ( sort( keys( %{ $c3{ $tc31 }{ $tc32 }{ $tc33 } } ) ) ) {  # Loop over lower nodes
                  my $n = keys( %{ $c3{ $tc31 }{ $tc32 }{ $tc33 }{ $ln } } );  # CID count
#                  print "3 :y: $n\t$tlabel\tBreakdown by $tc31\t$tc32\tYet more $tc32\tBreakdown by $ln\n";
                  if ( $sc_tc32 == 1 ) {
                    print "3 :y: $n\t$tlabel\tBreakdown by $tc31\t$tc32\tYet even more $ltc32\tBreakdown by $ln\n";
                  } else {
                    print "3 :y: $n\t$tlabel\tBreakdown by $tc31\t$tc32\tYet more $ltc32\tBreakdown by $ln\n";
                  }
                }
              } else {
                foreach my $ln ( sort( keys( %{ $c3{ $tc31 }{ $tc32 }{ $tc33 } } ) ) ) {  # Loop over lower nodes
                  foreach my $cid ( sort bynum( keys( %{ $c3{ $tc31 }{ $tc32 }{ $tc33 }{ $ln } } ) ) ) {
#                    print "$tlabel\tBreakdown by $tc31\t$tc32\tYet more $tc32\tBreakdown by $ln\t$cid\n";
                    if ( $sc_tc32 == 1 ) {
                      print "$tlabel\tBreakdown by $tc31\t$tc32\tYet even more $ltc32\tBreakdown by $ln\t$cid\n";
                    } else {
                      print "$tlabel\tBreakdown by $tc31\t$tc32\tYet more $ltc32\tBreakdown by $ln\t$cid\n";
                    }
                  }
                }
              }
            } else {
              $iym++;
              if ( $print_cid == 0 ) {
                foreach my $ln ( sort( keys( %{ $c3{ $tc31 }{ $tc32 }{ $tc33 } } ) ) ) {  # Loop over lower nodes
                  my $n = keys( %{ $c3{ $tc31 }{ $tc32 }{ $tc33 }{ $ln } } );  # CID count
#                  print "3 :y: $n\t$tlabel\tBreakdown by $tc31\t$tc32\tYet more $tc32\t$tc33\tBreakdown by $ln\n";
                  if ( $sc_tc32 == 1 ) {
                    print "3 :y: $n\t$tlabel\tBreakdown by $tc31\t$tc32\tYet even more $ltc32\t$tc33\tBreakdown by $ln\n";
                  } else {
                    print "3 :y: $n\t$tlabel\tBreakdown by $tc31\t$tc32\tYet more $ltc32\t$tc33\tBreakdown by $ln\n";
                  }
                }
              } else {
                foreach my $ln ( sort( keys( %{ $c3{ $tc31 }{ $tc32 }{ $tc33 } } ) ) ) {  # Loop over lower nodes
                  foreach my $cid ( sort bynum( keys( %{ $c3{ $tc31 }{ $tc32 }{ $tc33 }{ $ln } } ) ) ) {
#                    print "$tlabel\tBreakdown by $tc31\t$tc32\tYet more $tc32\t$tc33\tBreakdown by $ln\t$cid\n";
                    if ( $sc_tc32 == 1 ) {
                      print "$tlabel\tBreakdown by $tc31\t$tc32\tYet even more $ltc32\t$tc33\tBreakdown by $ln\t$cid\n";
                    } else {
                      print "$tlabel\tBreakdown by $tc31\t$tc32\tYet more $ltc32\t$tc33\tBreakdown by $ln\t$cid\n";
                    }
                  }
                }
              }
            }
          } else {
            if ( $print_cid == 0 ) {
              foreach my $ln ( sort( keys( %{ $c3{ $tc31 }{ $tc32 }{ $tc33 } } ) ) ) {  # Loop over lower nodes
                my $n = keys( %{ $c3{ $tc31 }{ $tc32 }{ $tc33 }{ $ln } } );  # CID count
                print "3 :: $n\t$tlabel\tBreakdown by $tc31\t$tc32\t$tc33\tBreakdown by $ln\n";
              }
            } else {
              foreach my $ln ( sort( keys( %{ $c3{ $tc31 }{ $tc32 }{ $tc33 } } ) ) ) {  # Loop over lower nodes
                foreach my $cid ( sort bynum( keys( %{ $c3{ $tc31 }{ $tc32 }{ $tc33 }{ $ln } } ) ) ) {
                  print "$tlabel\tBreakdown by $tc31\t$tc32\t$tc33\tBreakdown by $ln\t$cid\n";
                }
              }
            }
          }
        }
      }
    }
  }
}


# Check third case
my @c4 = sort( keys( %c4 ) );
my $nc4 = @c4;
if ( $nc4 > $max_node ) { print STDERR ":: WARNING :: Case of C4 1st w/ $nc4 :: ", join( "|", @c4 ), "\n"; }

foreach my $tc41 ( @c4 ) {
  my @tc42 = sort( keys( %{ $c4{ $tc41 } } ) );
  my $ntc42 = @tc42;
  if ( $ntc42 > $max_node ) {
    print STDERR ":: WARNING :: Case of C4 2nd w/ $ntc42 :: ", join( "|", @tc42 ), "\n";

    # Determine size of each node
    my %n = ();
    foreach my $tc42 ( @tc42 ) {
      my @tc43 = sort( keys( %{ $c4{ $tc41 }{ $tc42 } } ) );
      my $ntc43 = @tc43;

      foreach my $tc43 ( @tc43 ) {
        my @tc44 = sort( keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 } } ) );
        my $ntc44 = @tc44;
        foreach my $tc44 ( @tc44 ) {
          my $n44 = 0;
          foreach my $ln ( keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 } } ) ) {  # Loop over lower nodes
            my $n = keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 }{ $ln } } );  # CID count
            $n44 += $n;
          }
          $n{ $n44 }{ $tc44 } = undef;
        }
      }
    }
  }

  foreach my $tc42 ( @tc42 ) {
    my @tc43 = sort( keys( %{ $c4{ $tc41 }{ $tc42 } } ) );
    my $ntc43 = @tc43;
    if ( $ntc43 > $max_node ) { print STDERR ":: WARNING :: Case of C4 3rd w/ $ntc43 :: ", join( "|", @tc43 ), "\n"; }

    foreach my $tc43 ( @tc43 ) {
      my @tc44 = sort( keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 } } ) );
      my $ntc44 = @tc44;

      # Check for special cases
      my $ltc43 = $tc43;
      my $sc_tc43 = 0;
      if ( $tc43 =~ /More / ) {
        $sc_tc43 = 1;
        $ltc43 =~ s/More //;  # Yet even more ..
      } elsif ( $tc43 =~ /Yet more / ) {
        $sc_tc43 = 1;
        $ltc43 =~ s/Yet more //;  # Yet even more ..
      } elsif ( $tc43 =~ /Contains / ) {
        $ltc43 =~ s/Contains /contains /;  # Yet more contains ..
      }

      if ( $ntc44 <= $max_node ) {
        foreach my $tc44 ( @tc44 ) {
          my $n44 = 0;
          foreach my $ln ( keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 } } ) ) {  # Loop over lower nodes
            my $n = keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 }{ $ln } } );  # CID count
            $n44 += $n;
          }

          if ( $n44 < $min_cid ) {  # Too few CIDs .. truncate lower nodes
            if ( $print_cid == 0 ) {
              print "4 :t: $n44\t$tlabel\tBreakdown by $tc41\t$tc42\t$tc43\t$tc44\n";
            } else {
              foreach my $ln ( sort( keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 } } ) ) ) {  # Loop over lower nodes
                foreach my $cid ( sort bynum( keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 }{ $ln } } ) ) ) {
                  print "$tlabel\tBreakdown by $tc41\t$tc42\t$tc43\t$tc44\t$cid\n";
                }
              }
            }
          } else {
            if ( $print_cid == 0 ) {
              foreach my $ln ( sort( keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 } } ) ) ) {  # Loop over lower nodes
                my $n = keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 }{ $ln } } );  # CID count
                print "4 :: $n\t$tlabel\tBreakdown by $tc41\t$tc42\t$tc43\t$tc44\tBreakdown by $ln\n";
              }
            } else {
              foreach my $ln ( sort( keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 } } ) ) ) {  # Loop over lower nodes
                foreach my $cid ( sort bynum( keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 }{ $ln } } ) ) ) {
                  print "$tlabel\tBreakdown by $tc41\t$tc42\t$tc43\t$tc44\tBreakdown by $ln\t$cid\n";
                }
              }
            }
          }
        }
      } else {
        # Determine size of this node
        my %n = ();
        foreach my $tc44 ( @tc44 ) {
          my $n44 = 0;
          foreach my $ln ( keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 } } ) ) {  # Loop over lower nodes
            my $n = keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 }{ $ln } } );  # CID count
            $n44 += $n;
          }
          $n{ $n44 }{ $tc44 } = undef;
        }

        my $iym = 0;  # Track yet more cases to avoid too many nodes
        my $i1 = 0;  # Track count of cases
        my $i2 = 0;  # Track 'pages' of cases
        foreach my $n44 ( sort bynum( keys( %n ) ) ) {
          foreach my $tc44 ( sort( keys( %{ $n{ $n44 } } ) ) ) {
            $i1++;  # Track count of cases
            if ( ( $i1 % $max_node ) == 0 ) { $i2++; }  # Track 'pages' of cases

            if ( $i2 != 0 ) {
              if ( $n44 < $max_node_min_cid ||
                   $iym > $max_node ) {
                if ( $print_cid == 0 ) {
                  foreach my $ln ( sort( keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 } } ) ) ) {  # Loop over lower nodes
                    my $n = keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 }{ $ln } } );  # CID count
#                    print "4 :y: $n\t$tlabel\tBreakdown by $tc41\t$tc42\t$tc43\tYet more $tc43\tBreakdown by $ln\n";
                    if ( $sc_tc43 == 1 ) {
                      print "4 :y: $n\t$tlabel\tBreakdown by $tc41\t$tc42\t$tc43\tYet even more $ltc43\tBreakdown by $ln\n";
                    } else {
                      print "4 :y: $n\t$tlabel\tBreakdown by $tc41\t$tc42\t$tc43\tYet more $ltc43\tBreakdown by $ln\n";
                    }
                  }
                } else {
                  foreach my $ln ( sort( keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 } } ) ) ) {  # Loop over lower nodes
                    foreach my $cid ( sort bynum( keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 }{ $ln } } ) ) ) {
#                      print "$tlabel\tBreakdown by $tc41\t$tc42\t$tc43\tYet more $tc43\tBreakdown by $ln\t$cid\n";
                      if ( $sc_tc43 == 1 ) {
                        print "$tlabel\tBreakdown by $tc41\t$tc42\t$tc43\tYet even more $ltc43\tBreakdown by $ln\t$cid\n";
                      } else {
                        print "$tlabel\tBreakdown by $tc41\t$tc42\t$tc43\tYet more $ltc43\tBreakdown by $ln\t$cid\n";
                      }
                    }
                  }
                }
              } else {
                $iym++;
                if ( $print_cid == 0 ) {
                  foreach my $ln ( sort( keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 } } ) ) ) {  # Loop over lower nodes
                    my $n = keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 }{ $ln } } );  # CID count
#                    print "4 :y: $n\t$tlabel\tBreakdown by $tc41\t$tc42\t$tc43\tYet more $tc43\t$tc44\tBreakdown by $ln\n";
                    if ( $sc_tc43 == 1 ) {
                      print "4 :y: $n\t$tlabel\tBreakdown by $tc41\t$tc42\t$tc43\tYet even more $ltc43\t$tc44\tBreakdown by $ln\n";
                    } else {
                      print "4 :y: $n\t$tlabel\tBreakdown by $tc41\t$tc42\t$tc43\tYet more $ltc43\t$tc44\tBreakdown by $ln\n";
                    }
                  }
                } else {
                  foreach my $ln ( sort( keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 } } ) ) ) {  # Loop over lower nodes
                    foreach my $cid ( sort bynum( keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 }{ $ln } } ) ) ) {
#                      print "$tlabel\tBreakdown by $tc41\t$tc42\t$tc43\tYet more $tc43\t$tc44\tBreakdown by $ln\t$cid\n";
                      if ( $sc_tc43 == 1 ) {
                        print "$tlabel\tBreakdown by $tc41\t$tc42\t$tc43\tYet even more $ltc43\t$tc44\tBreakdown by $ln\t$cid\n";
                      } else {
                        print "$tlabel\tBreakdown by $tc41\t$tc42\t$tc43\tYet more $ltc43\t$tc44\tBreakdown by $ln\t$cid\n";
                      }
                    }
                  }
                }
              }
            } else {
              if ( $print_cid == 0 ) {
                foreach my $ln ( sort( keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 } } ) ) ) {  # Loop over lower nodes
                  my $n = keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 }{ $ln } } );  # CID count
                  print "4 :: $n\t$tlabel\tBreakdown by $tc41\t$tc42\t$tc43\t$tc44\tBreakdown by $ln\n";
                }
              } else {
                foreach my $ln ( sort( keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 } } ) ) ) {  # Loop over lower nodes
                  foreach my $cid ( sort bynum( keys( %{ $c4{ $tc41 }{ $tc42 }{ $tc43 }{ $tc44 }{ $ln } } ) ) ) {
                    print "$tlabel\tBreakdown by $tc41\t$tc42\t$tc43\t$tc44\tBreakdown by $ln\t$cid\n";
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}




sub bynum { $b <=> $a };

