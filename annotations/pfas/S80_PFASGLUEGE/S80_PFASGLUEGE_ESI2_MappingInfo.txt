Map_ID	Description
PFAAs	Perfluoroalkyl acids (PFAAs)
PFPIAs	Perfluoroalkyl phosphinic acids (PFPIA)-based substances
PASF	Perfluoroalkane sulfonyl fluoride (PASF)-based substances
PACF	Perfluoroalkane carbonyl fluoride (PACF)-based substances
FT	Flurotelomer based substances
CPFAS	Cyclic PFAS
O_Nonpoly	Other Nonpolymers
SCFA	Side-chain fluorinated aromatic
PFAE	Per- and Polyfluoroalkyl ether
HFE	Hydrofluoroether
NPP	Non polymer polymers
FP	Fluoropolymers
SCFP	Side-chain fluorinated polymers
PFPE	Perfluoroplyether
