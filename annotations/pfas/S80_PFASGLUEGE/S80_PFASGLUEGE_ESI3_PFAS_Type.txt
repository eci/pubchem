PFAS_Type	Display	PFAS_Type_tooltips
non-polymer	Non-polymer	Categorized as non-polymer in ESI-3 from Gluege et al 2020 DOI:10.1039/D0EM00291G and extracted from S80 PFASGLUEGE DOI:10.5281/zenodo.5029173
polymer	Polymer	Categorized as polymer in ESI-3 from Gluege et al 2020 DOI:10.1039/D0EM00291G and extracted from S80 PFASGLUEGE DOI:10.5281/zenodo.5029173. Note that the current classification is based on CIDs and InChIKeys and thus polymer entries are underrepresented. Please refer to original files. 
unclear	Unclear	Unclear PFAS polymer type in ESI-3 from Gluege et al 2020 DOI:10.1039/D0EM00291G and extracted from S80 PFASGLUEGE DOI:10.5281/zenodo.5029173
