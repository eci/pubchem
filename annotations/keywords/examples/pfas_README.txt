Notes based on communications with Zhanyun Wang

PFAS (prelim): SMARTs may be RCF2R' (R and R' cannot be H or X)
X=halogens
[!Cl,!Br,!I,!H]C(F)(F)[!Cl,!Br,!I,!H]
https://pubchem.ncbi.nlm.nih.gov/#query=%5B!Cl%2C!Br%2C!I%2C!H%5DC(F)(F)%5B!Cl%2C!Br%2C!I%2C!H%5D 
smartsview_prelimPFAS.png

CompTox PFASSTRUCT definition
https://comptox.epa.gov/dashboard/chemical_lists/PFASSTRUCT 
RCF2CFR'R' (R cannot be H)
[!H]C(F)(F)C(F)([!H])[!H]
https://pubchem.ncbi.nlm.nih.gov/#query=[!H]C(F)(F)C(F)([!H])[!H]&input_type=smarts
smartsview_CompToxPFAS.png

Previous notes from Evan:

Searching this in PubChem includes (basically) any Fluorine (CF3) containing compound .. and there is well north of 1M (est +10M):
FC([C,F])([C,F])
https://pubchem.ncbi.nlm.nih.gov/#query=FC(%5BC%2CF%5D)(%5BC%2CF%5D)&input_type=smarts&fullsearch=true&page=1

Here is a search using a (CF2)4 .. matching 67K in PubChem:
    https://pubchem.ncbi.nlm.nih.gov/#query=C(F)(F)C(F)(F)C(F)(F)C(F)(F)&tab=substructure&fullsearch=true&page=1

FC([C,F])([C,F])C([C,F])([C,F])
https://pubchem.ncbi.nlm.nih.gov/#query=FC(%5BC%2CF%5D)(%5BC%2CF%5D)C(%5BC%2CF%5D)(%5BC%2CF%5D)&input_type=smarts

