# UVCBs / Concepts

Note that the contents of this folder are "work in progress".

## Concepts and CIDs

The concept_cids folder contains mapping files (txt ending) containing the CIDs
that match to the concept XXXXX mentioned in the file name 
(concept_XXXXX_cids.txt). A csv file contains the mapping between 
concept name, concept number and matching OngLai series.

These CIDs were obtained by running the 
[OngLai](https://github.com/adelenelai/onglai-classify-homologues) 
algorithm on [PubChemLite](https://zenodo.org/record/7261187) 
to obtain UVCB matches with high annotation content.
The OngLai outputs were then run against concept names that mapped
to CIDs (under the theory that mapping CIDs were likely component hits). 
The matches were reviewed manually, and concepts were selected that are 
already on the dev site. 

The code associated with this work is currently under development in a 
private GitLab repository:
https://gitlab.lcsb.uni.lu/anjana.elapavalore/pubchem-concepts

This work is an outcome of collaborative efforts of Evan Bolton, Anjana Elapavalore and Emma Schymanski, as part of 
[Project 26](https://github.com/elixir-europe/biohackathon-projects-2022/tree/main/26) 
at 
[BioHackathon Europe 2022](https://biohackathon-europe.org/). 

## UVCB RegEx

The [regex](regex) folder contains the perl script and an extracted text file of the regular expressions
(RegEx) used to detect UVCB names in PubChem (developed first on the smaller sets PCL and SLE). 
See the presentation at DOI:[10.5281/zenodo.7883657](https://doi.org/10.5281/zenodo.7883657) for 
additional context. Note that hashed out lines indicate RegEx that were removed during validation 
(see slides for further details). 

This work is an outcome of collaborative efforts of Evan Bolton and Emma Schymanski, as part of 
[Project 26](https://github.com/elixir-europe/biohackathon-projects-2022/tree/main/26) 
at [BioHackathon Europe 2022](https://biohackathon-europe.org/). At this stage, please cite the
presentation above plus this repository if you use these RegEx. 
