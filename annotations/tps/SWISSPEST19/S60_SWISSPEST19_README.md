SourceDescription	
Swiss Government 2010	Swiss Government, 2010. Swiss Regulation No. 916.161 concerning the placment of plant protection products on the market (Verordnung �ber das Inverkehrbringen von Pflanzenschutzmitteln, Pflanzenschutzmittelverordnung, PSMV). 2005-2017. 
Eawag-Soil	www.envipath.org/package; Latino, D.A., Wicker, J., Gutlein, M., Schmid, E., Kramer, S. and Fenner, K., 2017. Eawag-Soil in enviPath: a new resource for exploring regulatory pesticide soil biodegradation pathways and half-life data. Environmental Science Processes and Impacts 19, 449-464.
Reemtsma et al. 2013	Reemtsma, T., Alder, L. and Banasiak, U., 2013. A multimethod for the determination of 150 pesticide metabolites in surface water and groundwater using direct injection liquid chromatography-mass spectrometry. Journal of chromatography A 1271(1), 95-104. DOI: 10.1016/j.chroma.2012.11.023
PPDB	Pesticides Properties DataBase; Lewis, K.A., Tzilivakis, J., Warner, D.J. and Green, A., 2016. An international database for pesticide risk assessments and management. Human and Ecological Risk Assessment: An International Journal 22(4), 1050-1064.
BLW 2017	BLW, 2017. Relevance of plant protection product metabolites in groundwater and drinkingwater (Relevanz von Pflanzenschutzmittel-Metaboliten im Grund- und Trinkwasser). Federal Office for the Agriculture, Agroscope, Federal Food Safety and Veterinary Office. https://www.blw.admin.ch/blw/de/home/nachhaltige-produktion/pflanzenschutz/pflanzenschutzmittel/nachhaltige-anwendung-und-risikoreduktion.html. Accessed: 18/01/2019.
T. Poiger	Personal communication, Thomas Poiger (Agroscope)
	
	
EvidenceDescription	
Kiefer et al 2019, New relevant pesticide transformation products in groundwater detected using target and suspect screening for agricultural and urban micropollutants with LC-HRMS, Water Research, Volume 165, 2019, 114972.
	
	
DatasetDescription	
S60 | SWISSPEST19 | Swiss Pesticides and Metabolites from Kiefer et al 2019.	

	
SOURCE:	
https://zenodo.org/record/3544760	
DOIs	
10.5281/zenodo.3544759 | 10.1016/j.watres.2019.114972	
