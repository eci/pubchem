---
title: "Updating HSDB Annotations"
author: "Emma Schymanski"
date: "6 October 2022"
output: word_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Background

This is an R Markdown document to detail how to update the HSDB Annotations dataset 
behind the original ShinyTPs code. This is taken from the Metabolism/Metabolites
section of the [HSDB Data Source](https://pubchem.ncbi.nlm.nih.gov/source/11933#data=Annotations) 
page in [PubChem](https://pubchem.ncbi.nlm.nih.gov/).

## Preparation

First, download and source extractAnnotations.R:

```{r prep work}
extr_anno_url <- "https://gitlab.lcsb.uni.lu/eci/pubchem/-/raw/master/annotations/tps/extractAnnotations.R?inline=false"
download.file(extr_anno_url,"extractAnnotations.R")
source("extractAnnotations.R")
library(httr)
```

## Retrieving HSDB Metabolism/Metabolites Annotation Contents 

The HSDB code is embedded as examples in extractAnnotations.R. Here we put it into runnable code.

First up, calculate the number of pages: 

```{r HSDB nPages}
n_pages <- getPcAnno.TotalPages('HSDB','Metabolism/Metabolites')
#n_pages <- getPcAnno.TotalPages('Hazardous Substances Data Bank (HSDB)','Metabolism/Metabolites')
#n_pages <- getPcAnno.TotalPages('Hazardous Substances Data Bank (HSDB)',
#                                'Metabolism/Metabolites (Complete)')
#n_pages <- getPcAnno.TotalPages("NORMAN Suspect List Exchange","Transformations")
n_pages
```

Now, try to retrieve all the annotation. Note that since the original code was 
drafted, the annotation content is now split into smaller files and "Complete" files, 
we want to retain the content from "Complete". Note, however, that not all will be displayed.

```{r HSDB retrieve data}
# This retrieves only the first page
# getPcAnno.Metabolism(source = 'HSDB',heading = 'Metabolism/Metabolites')
# This will retrieve all (in single files)
# n_pages <- getPcAnno.TotalPages('HSDB','Metabolism/Metabolites')
for (i in 1:n_pages) {
  getPcAnno.Metabolism(source = 'HSDB',heading = 'Metabolism/Metabolites (Complete)', page=i)
}
```

Now merge the files together:

```{r merge}
HSDB_anno <- read.csv("Metabolism_Metabolites (Complete)_HSDB_page1.csv")
HSDB_anno2 <- read.csv("Metabolism_Metabolites (Complete)_HSDB_page2.csv")
HSDB_anno_all <- merge(HSDB_anno,HSDB_anno2,all.x=TRUE,all.y=TRUE)

for (i in 3:n_pages) {
   file_name <- paste0("Metabolism_Metabolites (Complete)_HSDB_page",i,".csv")
   HSDB_anno2 <- read.csv(file_name)
   HSDB_anno_all <- merge(HSDB_anno_all,HSDB_anno2,all.x=TRUE,all.y=TRUE)
}

#write.csv(HSDB_data_all,"Metabolism_Metabolites_Complete_HSDB_merged.csv",row.names = F)
write.csv(HSDB_anno_all,"Metabolism_Metabolites_Complete_HSDB_allpages.csv",row.names = F)
```

This file has the same number of entries as previous files, but hopefully updated mappings. 

## Retrieve SourceID-CID mappings for HSDB

This is another function in extractAnnotations.R.

```{r HSDB Source ID} 
# This retrieves only the first page
# getPcAnno.SourceCIDs('HSDB','Hazardous%20Substances%20DataBank%20Number')
# Get all data
n_source_pages <- getPcAnno.TotalPages('HSDB','Hazardous%20Substances%20DataBank%20Number')
getPcAnno.SourceCIDs('HSDB','Hazardous%20Substances%20DataBank%20Number',page=1)
HSDB_data <- read.csv("HazardousSubstancesDataBankNumber_HSDB_page1.csv")

for (i in 2:n_source_pages) {
  getPcAnno.SourceCIDs('HSDB','Hazardous%20Substances%20DataBank%20Number',page=i)
}
HSDB_data <- read.csv("HazardousSubstancesDataBankNumber_HSDB_page1.csv")
HSDB_data2 <- read.csv("HazardousSubstancesDataBankNumber_HSDB_page2.csv")
HSDB_data_all <- merge(HSDB_data,HSDB_data2,all.x=TRUE,all.y=TRUE)

for (i in 3:n_source_pages) {
  file_name <- paste0("HazardousSubstancesDataBankNumber_HSDB_page",i,".csv")
  HSDB_data2 <- read.csv(file_name)
  HSDB_data_all <- merge(HSDB_data_all,HSDB_data2,all.x=TRUE,all.y=TRUE)
}

write.csv(HSDB_data_all,"HazardousSubstancesDataBankNumber_HSDB_allpages.csv", row.names = F)

```

Now, we need to merge these two files to create the final file with all information.

```{r ultimate merge}
#HSDB_data_all <- read.csv("HazardousSubstancesDataBankNumber_HSDB_allpages.csv",stringsAsFactors = F)
#HSDB_anno_all <- read.csv("Metabolism_Metabolites_Complete_HSDB_allpages.csv",stringsAsFactors = F)
HSDB_merged <- merge(HSDB_data_all,HSDB_anno_all,sort=F)
write.csv(HSDB_merged,"Metabolism_Metabolites_Complete_HSDB_merged.csv", row.names = F)
```

If this file is fine, we can update the Zenodo repository at
DOI:[10.5281/zenodo.7143229](https://doi.org/10.5281/zenodo.7143229).

One distinct change from previous file: the *source_name* column is now 
*Hazardous Substances Data Bank (HSDB)* instead of *HSDB*. 
This can be overwritten if problematic.


## Other notes

There are definitely optimizations that can be done with this extraction ... but hopefully 
this will generate files and allow us to update annotation content if necessary. 

It would be good to separate this from extractAnnotations.R and create a workflow with less overhead. The vast majority of the packages called at the beginning are not needed. 

It would probably be good to do a cleanup of the directory after creating the merged files, so 
that there aren't too many files on record. 
