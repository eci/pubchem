This subfolder is the home for code related to 
deposition and annotation of 
[MassBank.EU](https://massbank.eu/MassBank/) in
[PubChem](https://pubchem.ncbi.nlm.nih.gov/).

The MassBankEU_Export.R script runs on a local copy of 
[MassBank-data](https://github.com/MassBank/MassBank-data/)
and creates files for deposition and annotation. 


# Deposition

The file `MASSBANK_DATA_SUMMARY_MSMS_Unique_Substances.csv`
is specific to PubChem (has PubChem-specific headers) and 
has a few SMILES removed (see badlist files).
When newly generated, this should be copied to the ECI server 
`\Misc\from_mb_to_pb` where it should be detected and sent to 
the PubChem FTP server ... 

Should we need to revoke substances, just expand the 
[delete substances](https://pubchemdocs.ncbi.nlm.nih.gov/update-or-revoke-substances)
menu item in PubChem Docs to find out how. 


# Annotation

Updated annotation files should be deposited on the 
[PubChem-MassBank](https://doi.org/10.5281/zenodo.5139996) 
Zenodo record and `MassBank_File_Locations.txt` should be updated. 
If any fields change, update the Categories and Display Fields 
files too, if needed. Let Jeff know when updated files are available (updates are currently manual). 


# Contacts

- ECI: E. Schymanski, T. Kondic, A. Elapavalore 
- PubChem: E. Bolton, B. Shoemaker (deposition), J. Zhang (annotation)

